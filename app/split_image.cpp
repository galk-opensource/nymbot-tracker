#include <split_image.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <unordered_map>

int NW[4] = {1190, 3520, 50, 50};
int SW[4] = {1160, 190, 50, 50};
int NE[4] = {4125, 200, 50, 50};
int SE[4] = {4095, 3380, 50, 50};
int Center[4] = {2736, 1824, 50, 50};
string order[5] = {"NW", "NE", "Center", "SW", "SE"};
std::unordered_map<string, int*> zones = {{"NW", NW}, {"SW", SW}, {"SE", SE}, {"NE", NE}, {"Center", Center}};
bool selectObject = false;
cv::Rect selection;
cv::Point origin;
cv::Mat img, canvas;
string win_name = "Cropping Demo";
std::vector<cv::Rect> selections;

std::vector<cv::Mat> split_image(cv::Mat img, int n_images, std::unordered_map<string, int*> zones){
    std::vector<cv::Mat> ret;
    for (int i=0; i < n_images; i++){
        cv::Rect roi(zones[order[i]][0], zones[order[i]][1], zones[order[i]][2], zones[order[i]][3]);
        cv::Mat dst = img(roi);
        ret.push_back(dst);
    }
    return ret;
}

void calculate_focus(std::vector<cv::Mat> *images, double *sharpness){
    cv::Mat dst;
    cv::Scalar mu, sigma;
    // focus calculated from algorithm https://stackoverflow.com/questions/28717054/calculating-sharpness-of-an-image
    for(int i=0; i<(*images).size();i++) {
        
        cv::Laplacian((*images)[i], dst, (*images)[i].type());
        
        cv::meanStdDev(dst, mu, sigma);
        sharpness[i] = sigma.val[0] * sigma.val[0];
    }
}

cv::Mat concatenate_image(std::vector<cv::Mat> *images, cv::Size s){
    // currently hardcoded to show 5 images and one blank image
    cv::Mat hdst1,hdst2,vdst;
    cv::Mat empty = cv::Mat::zeros(s,(*images)[0].type());
    for(int i=0; i<(*images).size();i++) {
        cv::resize((*images)[i],(*images)[i], s);
    }
    std::vector<cv::Mat> images1 = std::vector<cv::Mat>((*images).begin(), (*images).end() - 3);
    images1.push_back(empty);
    std::vector<cv::Mat> images2 = std::vector<cv::Mat>((*images).begin() + 2, (*images).end());
    cv::hconcat(images1, hdst1);
    cv::hconcat(images2, hdst2);
    std::vector<cv::Mat> hdst = {hdst1,hdst2};
    cv::vconcat(hdst, vdst);
    return vdst;
}

int move_image(int key, std::unordered_map<string, int*> zones, int adjust_index, string file_output){
    if (adjust_index < 5){
        switch (key) {
            case 81: {
                zones[order[adjust_index]][0] += 1;
                return adjust_index;
            }
            case 82: {
                zones[order[adjust_index]][1] -= 1;
                return adjust_index;
            }
            case 83: {
                zones[order[adjust_index]][0] -= 1;
                return adjust_index;
            }
            case 84: {
                zones[order[adjust_index]][1] += 1;
                return adjust_index;
            }
            case 184: {
                zones[order[adjust_index]][3] -= 1;
                return adjust_index;
            }
            case 178: {
                zones[order[adjust_index]][3] += 1;
                return adjust_index;
            }
            case 180: {
                zones[order[adjust_index]][2] -= 1;
                return adjust_index;
            }
            case 182: {
                zones[order[adjust_index]][2] += 1;
                return adjust_index;
            }

            case 13: {
                adjust_index++;
                if (adjust_index == 5){
                    std::ofstream out(file_output);
                    for(int count = 0; count < 5; count ++){
                        out << order[count] << ": ";
                        out << zones[order[count]][0] << ", ";
                        out << zones[order[count]][1] << ", ";
                        out << zones[order[count]][2] << ", ";
                        out << zones[order[count]][3] << std::endl;
                        
                    }
                }
                return adjust_index;
            }
            default: {
                if (key != -1) {
                    cout << "key #" << key << " doesn't have assignment." << std::endl;
                }
            }
            // down = 84, up = 82 , right = 81 , left = 83, enter=13, keypad_up=184, keypad_down=178, 
            // keypad_left=180, keypad_right=182 
        }
    }
    return adjust_index;

}

cv::Mat write_text(int adjust_index, cv::Mat complete_img, double* sharpness, std::unordered_map<string, int*> zones){
    string txt;
    for(int i=0; i<5;i++) {
        if (adjust_index == 5) {
            std::stringstream ss;
            ss << std::fixed << std::setprecision(2) << sharpness[i];
            putText(complete_img, ss.str(), cv::Point((i%2)*350 + (i/4)*700 + 180,(i>=2)*345+350), 0,
            1.5, cv::Scalar(169, 82, 255,255), 3);
        }
        else
        {
            putText(complete_img, std::to_string(zones[order[adjust_index]][0]), cv::Point(880, 230), 0,
            1.5, cv::Scalar(169, 82, 255,255), 3);
            putText(complete_img, std::to_string(zones[order[adjust_index]][1]), cv::Point(880, 270), 0,
            1.5, cv::Scalar(169, 82, 255,255), 3);
            putText(complete_img, std::to_string(zones[order[adjust_index]][2]), cv::Point(880, 310), 0,
            1.5, cv::Scalar(169, 82, 255,255), 3);
            putText(complete_img, std::to_string(zones[order[adjust_index]][3]), cv::Point(880, 350), 0,
            1.5, cv::Scalar(169, 82, 255,255), 3);
        }
        putText(complete_img, order[i], cv::Point((i%2)*350 + (i/4)*700 + 110,(i>=2)*345+50), 0,
                1, cv::Scalar(169, 82, 255,255), 3);
    }

    return complete_img;
}

static bool showSelections()
{
    for (size_t i = 0; i< selections.size(); i++)
    {
        rectangle(canvas, selections[i], cv::Scalar(0, 255, 0), 2);
    }
    if (selectObject) {
        if (selection.width > 0 && selection.height > 0)
        {
            cv::Mat canvascopy;
            canvas.copyTo(canvascopy);
            cv::Mat roi = canvascopy(selection);
            cv::bitwise_not(roi, roi);
            cv::imshow(win_name, canvascopy);
        }
    }
    else
    {
        imshow(win_name, canvas);
    }   
    return true;
}

static void onMouse(int event, int x, int y, int, void*)
{
    switch (event)
    {
        case cv::EVENT_LBUTTONDOWN:
            origin = cv::Point(x, y);
            selectObject = true;
            break;
        case cv::EVENT_LBUTTONUP:
        {
            selectObject = false;
            selections.push_back(selection);
            showSelections();
            break;
        }
    }

    if (selectObject)
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x) + 1;
        selection.height = std::abs(y - origin.y) + 1;
    }
}


void read_file(string file_path, std::unordered_map<string, int*> zones) {
    
    string myText;
    ifstream MyReadFile(file_path);
    while (getline (MyReadFile, myText)) {
        std::string zone = myText.substr(0, myText.find(": ")); 
        myText.erase(0, myText.find(": ") + 2);
        std::string x = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string y = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string w = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string h = myText.substr(0, myText.find(", ")); 
        zones[zone][0] = std::stoi(x);
        zones[zone][1] = std::stoi(y);
        zones[zone][2] = std::stoi(w);
        zones[zone][3] = std::stoi(h);
    }
    MyReadFile.close(); 
}

int main(int argc, char** argv)
{
    std::string calibCamera = "";

    if (argc < 4){
        std::cout<< "args: <gstformat> <cameraJsonFile> <config_mode> <config_save_path>" <<std::endl;
        return 1;
    }

	VideoFile *vsptr;
    try {
		int camIndex = std::stoi(argv[1]);
		vsptr = new VideoFile(camIndex, calibCamera, argv[2]);
	}
	catch (const cv::Exception e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}
	catch (const std::exception& e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}

    VideoFile &vs = *vsptr;

    if (!vs.isOpened()){
        std::cout<< "video can't open."<<std::endl;
        return 1;
    }
    cv::Mat img;
    double sharpness[5];
    if(std::stoi(argv[2]) == 1) {
        while(vs.GetNextFrame(img)){
            cv::resize(img,img, cv::Size(800,800));
            img.copyTo(canvas);
            cv::namedWindow(win_name);
            cv::setMouseCallback(win_name, onMouse);
            int key = cv::waitKey(10);
            showSelections();
            if (key == 27){
                for (size_t i = 0; i < selections.size(); i++)
                {
                    zones[order[i]][0] = selections[i].x / 800.0 * 5472;
                    zones[order[i]][1] = selections[i].y / 800.0 * 3648;
                    zones[order[i]][2] = selections[i].width / 800.0 * 5472;
                    zones[order[i]][3] = selections[i].height / 800.0 * 3648;
                }
                cv::destroyWindow(win_name);
                break;
            }
            if (key == 's' && argc > 2) // crops and saves selections
            {
                std::ofstream out(argv[3]);
                for (size_t i = 0; i < selections.size(); i++)
                {       
                    out << order[i] << ": ";        
                    out << selections[i].x / 800.0 * 5472 << ", " << selections[i].y / 800.0 * 3648 << ", ";
                    out << selections[i].width / 800.0 * 5472 << ", " << selections[i].height / 800.0 * 3648 << std::endl;
                }
            }

            if ( (key == 'd') & (selections.size() > 0) ) // delete last selection
            {
                selections.erase(selections.end() -1 );
            }
        }

    } else {
        
        read_file(argv[3], zones);
    }    

    int adjust_index = 0;
    while(vs.GetNextFrame(img)){
        std::vector<cv::Mat> images = split_image(img, 5, zones);
        calculate_focus(&images, sharpness);
        cv::Mat complete_img = concatenate_image(&images, cv::Size(350,350));
        complete_img = write_text(adjust_index, complete_img, sharpness, zones);
        cv::imshow("image", complete_img);
        int key = cv::waitKey(10); 
        adjust_index = move_image(key, zones, adjust_index, argv[3]);
        
    }
    return 0;   
}