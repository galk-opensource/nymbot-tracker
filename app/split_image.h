#include <iostream>
#include "VideoFile.h"
#include "opencv2/opencv.hpp"


std::vector<cv::Mat> split_image(cv::Mat img, int n_images, int *xs, int *ys, int *ws, int *hs);