#include <iostream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"
extern "C" {
#include <nymbot_transmitter.h>
}


#define NYMBOTNUM 2
#define RADIUS 1070
#define MARGIN 250
#define OUTTER_THRESHOLD 100
#define NEW_NYMBOT_BOOST 20
#define NEW_NYMBOT_SPEED 36
#define OLD_NYMBOT_BOOST 5
#define OLD_NYMBOT_SPEED 32
/*
 * This program keeps track of each and every single nymbot location at a given frame
 * it comes to solve some "dead holes" which the camera does not identify the nymbot immediately
 * assuming the nymbots are not moving at a relatively fast speed the location and orientation of each nymbot
 * should be precise up to some degree.
 */
static bool initialized = false;
static struct nymbot_location_data {
    void printPos() const {
        cout << "Nymbot Position: x = " << x << ", y = " << y << endl;
        cout << "Nymbot Orientation = " << orientation << endl;
        cout << "Nymbot Angular Velocity = " << angular_velocity << endl;
        cout << "Nymbot Speed = " << speed << endl;
        cout << "Nymbot is using NYMBOT_BOOST value: " << default_angular_velocity << endl;
    }

    int x;
    int y;

    uint16_t orientation;
    int16_t angular_velocity = 0;
    uint16_t speed = NEW_NYMBOT_SPEED;

    bool is_managed = false;

    int16_t default_angular_velocity = NEW_NYMBOT_BOOST;
    uint8_t default_speed = NEW_NYMBOT_SPEED;
} nymbots_arr[ROBOT_MAX_NUMBER];

void LastSeenPosition(SceneData& sd) {
    uint8_t numOfLocusts= sd.Locusts.size();
    for (int i = 0; i < numOfLocusts; ++i) {
        Locust l = sd.Locusts.at(i);
        if(l.m_Marker.isValid()) {
            nymbots_arr[i].orientation = l.GetOrientation();
            nymbots_arr[i].x = l.GetPositionIJ().x;
            nymbots_arr[i].y = l.GetPositionIJ().y;
        }
    }
    if(initialized)
        return;
    else {
        for (int i = 0; i < ROBOT_MAX_NUMBER; i++) {
            nymbot_location_data* nymdata = &nymbots_arr[i];
            nymdata->orientation = USHRT_MAX;
            nymdata->x = INT32_MIN;
            nymdata->y = INT32_MIN;
            nymdata->angular_velocity = 0;
            if(i == 2 || i == 3 || i == 5) {
                nymdata->default_angular_velocity = OLD_NYMBOT_BOOST;
                nymdata->default_speed = OLD_NYMBOT_SPEED;
            }
        }
        initialized = true;
    }
}
// UP until here we dealt with identifying all nymbots on the grid and registering their position and orientation
// Now we want a single nymbot (3 for this demonstration to run in circle around a given center point
// The center point will be considered to be the marker on the nymbot placed in the middle after that we can hard
// code it if we'd like

static cv::Point circleCenter(2510, 1803);

// Illustration of the box coordinates
/*
 * NOTICE the values can be changed due to physical move of the arena this is only for reference
             DOWN // Y values here are smaller
  (943,90)   - - - - - - - - - - - - - - - - - - -  (4305, 199)//X VALUES HERE ARE GREATER
            |                                     | RIGHT
            |   270<-                    ^360|0   |
            |                                     |
X VALUES    |             = = = = =               |
HERE  ARE   |          =            =             |
SMALLER     |        =                =           |
            |        =  (2509, 1804)   =          |
            |        =                =           |
            |          =             =            |
            |   |          = = = = =              |
       LEFT |  \/ 180                     ->90    |
(812, 3509)  - - - - - - - - - - - - - - - - - - - (4176, 3578)
                                  UP // Y values here are greater
*/



/*
 * Given the direction the nymbot is facing and its relative position to the circle we will need to deduce
 * what should be the angular speed, the speed will be constant at SPEED.
 * using the nymbot_location_data angular velocity we can assign each nymbot with its own(!!) angular velocity.
 * keeping this information within nymbot_location_data will make the function address each nymbot as a unique nymbot
 * and not thinking about them as a group.
 */

bool inCorridorAgainstRadius(uint8_t nymbotID) {
    nymbot_location_data nym_data = nymbots_arr[nymbotID];
    float distanceFromCenter = sqrt(pow(nym_data.x - circleCenter.x, 2) + pow(nym_data.y - circleCenter.y, 2));
    return (distanceFromCenter < RADIUS + MARGIN) && (distanceFromCenter > RADIUS - MARGIN);
}

bool inCorridorAgainstOutterRadius(uint8_t nymbotID) {
    // 1 => Position is OKAY
    // 0 => Need to start correcting
    float outer_radius = RADIUS + MARGIN;
    nymbot_location_data nym_data = nymbots_arr[nymbotID];
    float distanceFromCenter = sqrt(pow(nym_data.x - circleCenter.x, 2) + pow(nym_data.y - circleCenter.y, 2));
    bool result = outer_radius - distanceFromCenter >= OUTTER_THRESHOLD;
    return result;
}


void AngularVelocityCorrection(uint8_t nymbotID) { // ASSUMING CLOCKWISE (RIGHT TURNING)
    nymbot_location_data* nym_data = &nymbots_arr[nymbotID];
    if(nym_data->is_managed)
        return;
    nym_data->angular_velocity = nym_data->default_angular_velocity;
    nym_data->speed = nym_data->default_speed;
    nym_data->is_managed = true;
}

void SetDefaultAngularVelocity(int8_t nymbotID) { // finished correcting the nymbot
    nymbot_location_data* nym_data = &nymbots_arr[nymbotID];
    if(nym_data->is_managed) {
        nym_data->angular_velocity = 0;
        nym_data->speed = nym_data->default_speed;
        nym_data->is_managed = false;
    }
}

void OnFrame(SceneData sd) {
    #ifdef DEBUG
    cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
    #endif

    cv::Mat img = sd.LastFrame.clone();

    LastSeenPosition(sd);

    if(sd.FrameNo % 3 == 0) {
        if(inCorridorAgainstOutterRadius(NYMBOTNUM)) {
            SetDefaultAngularVelocity(NYMBOTNUM);
        } else {
            AngularVelocityCorrection(NYMBOTNUM);
        }
        nymbot_update_velocity(NYMBOTNUM, nymbots_arr[NYMBOTNUM].speed, nymbots_arr[NYMBOTNUM].angular_velocity);

    }
    for (auto& l : sd.Locusts)
    {
        if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
            continue;
        l.m_Marker.draw(img);
        int id = l.m_Marker.id;
        cv::Point p(l.GetPositionIJ());
        int orientation = l.GetOrientation();
        //cv::Point2d xy = sd.GetPositionXY(l);
        cv::putText(img, //target image
                    cv::format("%d,(%d,%d),%d", id, p.x, p.y, orientation), //text
                    p, //top-left position
                    cv::FONT_HERSHEY_DUPLEX,
                    2.0,
                    CV_RGB(118, 185, 0), //font color
                    3);
    }
    //cv::Mat map = sd.GetMap();
    cv::resize(img, img, cv::Size(1200, 600));
    cv::imshow("image", img);
    int waitTime = 1;
    char key = 0;
    key = cv::waitKey(waitTime);  // wait for key to be pressed
    if (key == 's')
        waitTime = waitTime == 0 ? 1 : 0;
}

int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
	string cameraJsonFile;

    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");

    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    AsyncDetector d(configFile, vs);
    d.Start();
    threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames();
    T_RES res =nymbot_init("/dev/ttyUSB0");
    if(res != T_RES_SUCCESS)
        return -1;

    while (d.IsProcessing()) {
        std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();
        OnFrame(*frame);
    }

    return 0;
}
