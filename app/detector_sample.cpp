
using namespace std;

#include <iostream>
#include "Detector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"



void OnFrame(SceneData sd) {

	cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
	cv::Mat img = sd.LastFrame.clone();
	for (auto& l : sd.Locusts)
	{
		if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
			continue;
		l.m_Marker.draw(img);
		int id = l.m_Marker.id;
		cv::Point p(l.GetPositionIJ());
		int orientation = l.GetOrientation();
		//cv::Point2d xy = sd.GetPositionXY(l);
		cv::putText(img, //target image
			cv::format("%d,(%d,%d),%d", id, p.x, p.y, orientation), //text
			p, //top-left position
			cv::FONT_HERSHEY_DUPLEX,
			1.0,
			CV_RGB(118, 185, 0), //font color
			2);
	}
	

	cv::resize(img, img, cv::Size(1200, 800));
	cv::imshow("image", img);
	int waitTime = 10;
	char key = 0;
	key = cv::waitKey(waitTime);  // wait for key to be pressed
	if (key == 's')
		waitTime = waitTime == 0 ? 10 : 0;

}


int main(int argc, char** argv)
{
	CmdLineParser cml(argc, argv);
	string calibCamera;
	string configFile;
	string cameraJson;

	if (argc < 2 || cml["-h"])
	{
		cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml]" << std::endl;
		return 0;
	}
	if (cml["-c"])
		calibCamera = cml("-c");
	if (cml["-config"])
		configFile = cml("-config");
	if (cml["-cameraJson"])
		cameraJson = cml("-cameraJson");
		
	VideoSource* vs;

	try {
		int camIndex = std::stoi(argv[1]);
		vs = new VideoFile(camIndex, calibCamera, cameraJson);
	}
	catch (const cv::Exception e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJson);
	}
	catch (const std::exception& e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJson);
	}
	
	if (!vs->isOpened()) throw std::runtime_error("Could not open video");

	Detector dd(configFile, vs, OnFrame);
	dd.Start();
	return 0;
}
