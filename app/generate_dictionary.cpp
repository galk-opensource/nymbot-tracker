﻿#include <opencv2/core.hpp>
#include "opencv2/core/hal/hal.hpp"
#include "opencv2/aruco/dictionary.hpp"
#include <opencv2/aruco.hpp>
#include "CmdLineParser.h"
#include <fstream>
using namespace cv;
using namespace cv::aruco;

int main(int argc, char** argv)
{
    CmdLineParser cml(argc, argv);
    string configFile;

    if (argc < 3 || cml["-h"])
    {
        cerr << "Usage: dictionary_size  marker_size output_dictionary_file_path_" << std::endl;
        cout << "Example: 250 4 Aruco4X4_250.dict" << endl << endl;
        return 0;
    }
  
    int dictSize = stoi(argv[1]), markerSize = stoi(argv[2]);
    Ptr<Dictionary> dict = generateCustomDictionary(dictSize, markerSize);

    std::ofstream out(argv[3]);
    if (!out.is_open())
        std::cerr << "cannot open file for write: " << argv[3] << endl;
    out << "name BIU" << endl;
    out << "nbits  " << markerSize * markerSize << endl;
    for (size_t i = 0; i < dictSize; i++)
    {
        Mat markerBytes = dict->bytesList.rowRange(i, i + 1);
        Mat markerBits = Dictionary::getBitsFromByteList(markerBytes, sqrt(markerBytes.total()*8));
        uchar* arr = markerBits.isContinuous() ? markerBits.data : markerBits.clone().data;
        uint length = markerBits.total() * markerBits.channels();
        for (size_t j = 0; j < length; j++)
            out << int(arr[j]);
        out << endl;
        Mat markerImg;
        aruco::drawMarker(dict, i, markerSize + 2, markerImg, 1);
    }
    out.close();
    return 0;
}