#include <iostream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"
#include <nymbot_transmitter.h>

/*
 * This program keeps track of each and every single nymbot location at a given frame
 * it comes to solve some "dead holes" which the camera does not identify the nymbot immediately
 * assuming the nymbots are not moving at a relatively fast speed the location and orientation of each nymbot
 * should be precise up to some degree.
 *
 */
static bool initialized = false;
static struct nymbot_location_data {
    int x;
    int y;
    uint16_t orientation;
} nymbots_arr[ROBOT_MAX_NUMBER];

void LastSeenPosition(SceneData& sd) {
    uint8_t numOfLocusts= sd.Locusts.size();
    for (int i = 0; i < numOfLocusts; ++i) {
        Locust l = sd.Locusts.at(i);
        if(l.m_Marker.isValid()) {
            nymbots_arr[i].orientation = l.GetOrientation();
            nymbots_arr[i].x = l.GetPositionIJ().x;
            nymbots_arr[i].y = l.GetPositionIJ().y;
        }
    }
    if(initialized)
        return;
    else {
        for (nymbot_location_data nymdata: nymbots_arr) {
            nymdata.orientation = USHRT_MAX;
            nymdata.x = INT32_MIN;
            nymdata.y = INT32_MIN;
        }
        initialized = true;
    }
}

void OnFrame(SceneData sd) {
    cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
    cv::Mat img = sd.LastFrame.clone();
    for (auto& l : sd.Locusts)
    {
        if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
            continue;
        l.m_Marker.draw(img);
        int id = l.m_Marker.id;
        cv::Point p(l.GetPositionIJ());
        int orientation = l.GetOrientation();
        //cv::Point2d xy = sd.GetPositionXY(l);
        cv::putText(img, //target image
            cv::format("%d,(%d,%d),%d", id, p.x, p.y, orientation), //text
            p, //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            2.0,
            CV_RGB(118, 185, 0), //font color
            3);
    }
    //cv::Mat map = sd.GetMap();
    cv::resize(img, img, cv::Size(1200, 600));
    cv::imshow("image", img);
    int waitTime = 1;
    char key = 0;
    key = cv::waitKey(waitTime);  // wait for key to be pressed
    if (key == 's')
        waitTime = waitTime == 0 ? 1 : 0;
    if(sd.FrameNo % 10 == 0)
        LastSeenPosition(sd);


}

int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
	string cameraJsonFile;
    
    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");

    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    AsyncDetector d(configFile, vs);
    d.Start();

    threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames();

    while (d.IsProcessing()) {
        std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();
        OnFrame(*frame);
    }

    return 0;
}
