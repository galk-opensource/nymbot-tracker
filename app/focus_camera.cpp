#include <iostream>
#include "VideoFile.h"
#include "opencv2/opencv.hpp"

int main(int argc, char** argv)
{
    std::string calibCamera = "";

    if (argc <2){
        std::cout<< "args: <gstformat> <configFile>" <<std::endl;
        return 1;
    }

	VideoFile *vsptr;

    try {
		int camIndex = std::stoi(argv[1]);
		vsptr = new VideoFile(camIndex, calibCamera, argv[2]);
	}
	catch (const cv::Exception e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}
	catch (const std::exception& e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}

    VideoFile &vs = *vsptr;

    if (!vs.isOpened()){
        std::cout<< "video can't open."<<std::endl;
        return 1;
    }
    int width = 50;
    int height = 50;
    cv::Mat img;
    while(vs.GetNextFrame(img)){
        int x = img.cols/2;
        int y = img.rows/2;

        cv::Rect roi(x, y, width, height);
        cv::Mat dst = img(roi);
        
        cv::resize(dst, dst, cv::Size(1200, 1200));

	    cv::imshow("image", dst);
        int waitTime = 10;
        char key = 0;
        key = cv::waitKey(waitTime);  // wait for key to be pressed
        if (key == 's')
            waitTime = waitTime == 0 ? 10 : 0;
    }


    return 0;   
}