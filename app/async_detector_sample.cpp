#include <iostream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"

void OnFrame(SceneData sd) {
    vector<cv::Point> pastPos;
	cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
	cv::Mat img = sd.LastFrame.clone();
	for (auto& l : sd.Locusts)
	{
		if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
			continue;
		l.m_Marker.draw(img);
		int id = l.m_Marker.id;
		cv::Point p1(l.GetPositionIJ());
		pastPos.push_back(p1);
		int orientation = l.GetOrientation();
		//cv::Point2d xy = sd.GetPositionXY(l);
		for(auto& p : pastPos){
            cv::putText(img, //target image
                        cv::format("*"), //text
                        p, //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        1.0,
                        CV_RGB(118, 185, 0), //font color
                        2);
        }
	}
	//cv::Mat map = sd.GetMap();

	cv::resize(img, img, cv::Size(1200, 600));
	cv::imshow("image", img);
	int waitTime = 1;
	char key = 0;
	key = cv::waitKey(waitTime);  // wait for key to be pressed
	if (key == 's')
		waitTime = waitTime == 0 ? 1 : 0;
}

int main(int argc,char** argv)
{
	CmdLineParser cml(argc, argv);
	string calibCamera;
	string configFile;
	string cameraJsonFile;

	if (argc < 2 || cml["-h"])
	{
		cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
		return 0;
	}
	if (cml["-c"])
		calibCamera =cml("-c");
	if (cml["-config"])
		configFile =cml("-config");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");

	VideoFile* vs;

	try {
		int camIndex = std::stoi(argv[1]);
		vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
	}
	catch (const cv::Exception e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
	}
	catch (const std::exception& e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
	}
	if (!vs->isOpened()) throw std::runtime_error("Could not open video");
	 
	cv::Size camSize = vs->GetCameraParameters().CamSize;
	bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
	writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);
	
	AsyncDetector d(configFile, vs);
	d.Start();

	threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames();

	while (d.IsProcessing()) {
		std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();
		OnFrame(*frame);
	}
	
	return 0;
}
