INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src)
find_package( OpenCV REQUIRED )
#find_package(aruco REQUIRED PATHS ${CMAKE_BINARY_DIR}/aruco)
if(CMAKE_COMPILER_IS_GNUCXX OR MINGW OR ${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)
SET(THREADLIB "pthread")
ENDIF()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(nymbot_infrastructe ${NYMBOT_PROJECT}/nymbot-infrastructure)

include_directories( ${OpenCV_INCLUDE_DIRS} ${aruco_INCLUDE_DIRS} ${nymbot_infrastructe}/include)
message (STATUS "OpenCV_INCLUDE_DIRS ${OpenCV_INCLUDE_DIRS}")
message (STATUS "OpenCV_INCLUDE_DIRS ${aruco_INCLUDE_DIRS}")
message (STATUS "aruco_INCLUDE_DIRS ${aruco_INCLUDE_DIRS}")
message (STATUS "OpenCV_LIBS ${OpenCV_LIBS}")
message (STATUS "aruco_LIB_DIR ${aruco_LIB_DIR}")
message (STATUS "aruco_LIBS ${aruco_LIBS}")
message (STATUS "aruco ${CMAKE_BINARY_DIR}")
message (STATUS "aruco ${CMAKE_BINARY_DIR}/lib/aruco")
message (STATUS "nymbot-infrastructure headers ${nymbot_infrastructe}/include")
set(aruco_LIB_DIR ${CMAKE_BINARY_DIR}/lib/aruco)

               # <--this is out-file path

add_executable(video_splitter video_splitter.cpp)
target_link_libraries(video_splitter Detector ${OpenCV_LIBS} aruco ${THREADLIB})

add_executable(split_image split_image.cpp)
target_link_libraries(split_image Detector ${OpenCV_LIBS} aruco ${THREADLIB})

add_executable(focus_camera focus_camera.cpp)
target_link_libraries(focus_camera Detector ${OpenCV_LIBS} aruco ${THREADLIB})

add_executable(camera_calibration camera_calibration.cpp)
target_link_libraries(camera_calibration ${OpenCV_LIBS} ${THREADLIB})


add_executable(generate_dictionary generate_dictionary.cpp)
target_link_libraries(generate_dictionary Detector ${OpenCV_LIBS} aruco ${THREADLIB})

add_executable(arena_calibration arena_calibration.cpp)
target_link_libraries(arena_calibration Detector ${OpenCV_LIBS} aruco ${THREADLIB})

add_executable(print_dictionary print_dictionary.cpp)
target_link_libraries(print_dictionary Detector ${OpenCV_LIBS} aruco ${THREADLIB})



link_directories(detector_sample PUBLIC ${aruco_LIB_DIR})
add_executable(detector_sample detector_sample.cpp)
target_link_libraries(detector_sample Detector ${OpenCV_LIBS} aruco ${THREADLIB})
#link_directories(detector_sample PUBLIC ${aruco_LIB_DIR} )

add_executable(async_detector_sample async_detector_sample.cpp)
target_link_libraries(async_detector_sample Detector ${OpenCV_LIBS} aruco ${THREADLIB}) 

#add_executable(nymbot_calibration nymbot_calibration.cpp)
#target_link_libraries(nymbot_calibration Detector ${OpenCV_LIBS} aruco ${THREADLIB} nymbot_transmitter ${nymbot_infrastructe}/lib/libnymbot_transmitter.so)

#add_executable(selected_nymbot_calibration selected_nymbot_calibration.cpp)
#target_link_libraries(selected_nymbot_calibration Detector ${OpenCV_LIBS} aruco ${THREADLIB} nymbot_transmitter ${nymbot_infrastructe}/lib/libnymbot_transmitter.so)

#add_executable(single_robot_circle single_robot_circle.cpp)
#target_link_libraries(single_robot_circle Detector ${OpenCV_LIBS} aruco ${THREADLIB} nymbot_transmitte ${nymbot_infrastructe}/lib/libnymbot_transmitter.so)

#add_executable(barcode_to_id_map barcode_to_id_map.cpp)
#target_link_libraries(barcode_to_id_map Detector ${OpenCV_LIBS} aruco ${THREADLIB} nymbot_transmitter ${nymbot_infrastructe}/lib/libnymbot_transmitter.so)

if("${CMAKE_GENERATOR}" MATCHES "(Win64|IA64)")

#copy aruco dll
add_custom_command(TARGET detector_sample POST_BUILD        # Adds a post-build event to MyTest
    COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
        "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/aruco/aruco3112.dll"      # <--this is in-file
        ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}) 
#copy opencv dll
get_target_property(__dll_dbg opencv_world IMPORTED_LOCATION_DEBUG)
get_target_property(__dll_release opencv_world  IMPORTED_LOCATION_RELEASE)


message (STATUS "TZZZ  ${__dll_dbg}")
message (STATUS "TZZZ  ${__dll_release}")

add_custom_command(TARGET detector_sample POST_BUILD        # Adds a post-build event the TARGET
    COMMAND ${CMAKE_COMMAND} -E copy_if_different           # which executes "cmake - E copy_if_different..."
    "$<$<CONFIG:debug>:${__dll_dbg}>$<$<CONFIG:release>:${__dll_release}>"      # <--this is in-file
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}                        # <--this is out-file path
        # another dll copy if needed here
    COMMENT "    [1718_34_ATK] copy dlls realsense2 and opencv_world")


endif()
