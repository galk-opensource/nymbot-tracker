#include <split_image.h>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <unordered_map>
#include <chrono>



int NW[4] = {1190, 3520, 50, 50};
int SW[4] = {1160, 190, 50, 50};
int NE[4] = {4125, 200, 50, 50};
int SE[4] = {4095, 3380, 50, 50};
int Center[4] = {2736, 1824, 50, 50};
string order[5] = {"NW", "NE", "Center", "SW", "SE"};
std::unordered_map<string, int*> zones = {{"NW", NW}, {"SW", SW}, {"SE", SE}, {"NE", NE}, {"Center", Center}};

std::vector<cv::Mat> split_image(cv::Mat img, int n_images, std::unordered_map<string, int*> zones){
    std::vector<cv::Mat> ret;
    for (int i=0; i < n_images; i++){
        cv::Rect roi(zones[order[i]][0], zones[order[i]][1], zones[order[i]][2], zones[order[i]][3]);
        cv::Mat dst = img(roi);
        ret.push_back(dst);
    }
    return ret;
}

cv::Mat concatenate_image(std::vector<cv::Mat> *images, cv::Size s){
    // currently hardcoded to show 5 images and one blank image
    cv::Mat hdst1,hdst2,vdst;
    std::vector<cv::Mat> dst(*images);
    cv::Mat empty = cv::Mat::zeros(s,dst[0].type());
    for(int i=0; i<dst.size();i++) {
        cv::resize(dst[i],dst[i], s);
    }
    std::vector<cv::Mat> images1 = std::vector<cv::Mat>(dst.begin(), dst.end() - 3);
    images1.push_back(empty);
    std::vector<cv::Mat> images2 = std::vector<cv::Mat>(dst.begin() + 2, dst.end());
    cv::hconcat(images1, hdst1);
    cv::hconcat(images2, hdst2);
    std::vector<cv::Mat> hdst = {hdst1,hdst2};
    cv::vconcat(hdst, vdst);
    return vdst;
}

cv::Mat write_text(cv::Mat complete_img, std::unordered_map<string, int*> zones){
    string txt;
    for(int i=0; i<5;i++) {
        putText(complete_img, order[i], cv::Point((i%2)*350 + (i/4)*700 + 110,(i>=2)*345+50), 0,
                1, cv::Scalar(169, 82, 255,255), 3);
    }

    return complete_img;
}


void read_file(string file_path, std::unordered_map<string, int*> zones) {
    
    string myText;
    ifstream MyReadFile(file_path);
    while (getline (MyReadFile, myText)) {
        std::string zone = myText.substr(0, myText.find(": ")); 
        myText.erase(0, myText.find(": ") + 2);
        std::string x = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string y = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string w = myText.substr(0, myText.find(", ")); 
        myText.erase(0, myText.find(", ") + 2);
        std::string h = myText.substr(0, myText.find(", ")); 
        zones[zone][0] = std::stoi(x);
        zones[zone][1] = std::stoi(y);
        zones[zone][2] = std::stoi(w);
        zones[zone][3] = std::stoi(h);
    }
    MyReadFile.close(); 
}

int main(int argc, char** argv)
{
    std::string calibCamera = "";

    if (argc < 4){
        std::cout<< "args: <gstformat> <cameraJson> <config_save_path> <video_file_name> <seconds_to_record>" <<std::endl;
        return 1;
    }

	VideoFile *vsptr;
    try {
		int camIndex = std::stoi(argv[1]);
		vsptr = new VideoFile(camIndex, calibCamera, argv[2]);
	}
	catch (const cv::Exception e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}
	catch (const std::exception& e) {
		vsptr = new VideoFile(argv[1], calibCamera, argv[2]);
	}

    VideoFile &vs = *vsptr;

    if (!vs.isOpened()){
        std::cout<< "video can't open."<<std::endl;
        return 1;
    }
    int stop_time;
    if (argc > 5){
        stop_time = std::stoi(argv[5]);
    }
    else {
        stop_time = -1;
    }
    cv::Mat img;

    read_file(argv[3], zones);

    int frame_width = 5472; //get the width of frames of the video
    int frame_height = 3648; //get the height of frames of the video
    
    cv::Size frame_size(frame_width, frame_height);
    int frames_per_second = 18;

    //Create and initialize the VideoWriter object 
    cv::VideoWriter oVideoWriterNW(cv::format("%s_NW.avi", argv[4]), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 
                                                               frames_per_second, cv::Size(zones["NW"][2], zones["NW"][3]), true); 

    cv::VideoWriter oVideoWriterNE(cv::format("%s_NE.avi", argv[4]), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 
                                                               frames_per_second, cv::Size(zones["NE"][2], zones["NE"][3]), true); 

    cv::VideoWriter oVideoWriterCenter(cv::format("%s_Center.avi", argv[4]), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 
                                                               frames_per_second, cv::Size(zones["Center"][2], zones["Center"][3]), true); 

    cv::VideoWriter oVideoWriterSW(cv::format("%s_SW.avi", argv[4]), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 
                                                            frames_per_second, cv::Size(zones["SW"][2], zones["SW"][3]), true); 

    cv::VideoWriter oVideoWriterSE(cv::format("%s_SE.avi", argv[4]), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 
                                                            frames_per_second, cv::Size(zones["SE"][2], zones["SE"][3]), true); 

    std::vector<cv::VideoWriter> video_writers = {oVideoWriterNW, oVideoWriterNE, oVideoWriterCenter, oVideoWriterSW, oVideoWriterSE};

    if (oVideoWriterNW.isOpened() == false || oVideoWriterNE.isOpened() == false || oVideoWriterCenter.isOpened() == false 
    || oVideoWriterSW.isOpened() == false || oVideoWriterSE.isOpened() == false ) 
    {
        cout << "Cannot save the video to a file" << endl;
        cin.get(); //wait for any key press
        return -1;
    }
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point end;
    while(vs.GetNextFrame(img)){
        std::vector<cv::Mat> images = split_image(img, 5, zones);
        cv::Mat complete_img = concatenate_image(&images, cv::Size(350,350));
        complete_img = write_text(complete_img, zones);
        
        
        for (int i=0; i<5;i++){
            video_writers[i].write(images[i]);
        }

        cv::imshow("image", complete_img);
        int key = cv::waitKey(10); 
        if (key == 27){
            cout << "Esc key is pressed by the user. Stopping the video" << endl;
            break;
        }
        end = std::chrono::steady_clock::now();
        
        if (stop_time < (std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count()) /1000000.0 && stop_time != -1){
            cout << stop_time << " seconds have elapsed. Stopping the video" << endl;
            break;
        }
        
    }
    for (int i=0; i<5;i++){
        video_writers[i].release();
    }
    return 0;   
}