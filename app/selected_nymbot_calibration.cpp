#include <iostream>
#include <fstream>
#include <timers.h>
#include <string>
extern "C" {
#include <nymbot_transmitter_low.h>
}
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"

#define FULL_ENGINE_PWR 0
#define STOP 100, 100
#define RECORD_TIME(id, type) current_working_nymbot.type.end();
#define TEST_TIME 4
#define CALIBRATION_DATA_FILE_NAME "test_calibration.txt"
#define TEMP_CALIBRATION_DATA_FILE_NAME "temp_calibration.txt"
typedef enum
{
    NO_ENGINE,
    LEFT_ENGINE,
    RIGHT_ENGINE
} ENGINE;

typedef enum
{
    GET_NYMBOT_ID_TO_CALIBRATE,
    ATTEMPT_CALIBRATE_SELECTED_NYMBOT,
    SELECT_ENGINE_TO_CALIBRATE,
    IS_NYMBOT_CALIBRATED,
    WRITE_DATA_TO_FILE,
    END_PROGRAM
} ProgramState;

static struct nymbot_location_data {
    int initialOrientation, finalOrientation, delta;
    int l_engine_pwr = -1, r_engine_pwr = -1;
    ENGINE engine_to_fix;
    Timer t;
    bool handled = false;
    bool first_time = true;

} nymbots_arr[ROBOT_MAX_NUMBER];

static int in_calibration_nymbot = 0;
static ProgramState ps = GET_NYMBOT_ID_TO_CALIBRATE;

void displayImage(SceneData& sd)
{
    vector<cv::Point> pastPos;
    cv::Mat img = sd.LastFrame.clone();
    for (auto& l : sd.Locusts)
    {
        if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
            continue;
        l.m_Marker.draw(img);
        int id = l.m_Marker.id;
        cv::Point p1(l.GetPositionIJ());
        pastPos.push_back(p1);
        int orientation = l.GetOrientation();
        //cv::Point2d xy = sd.GetPositionXY(l);
        for(auto& p : pastPos){
            cv::putText(img, //target image
                        cv::format("*"), //text
                        p, //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        1.0,
                        CV_RGB(118, 185, 0), //font color
                        2);
        }
    }
    //cv::Mat map = sd.GetMap();

    cv::resize(img, img, cv::Size(1200, 600));
    cv::imshow("image", img);
    int waitTime = 1;
    char key = 0;
    key = cv::waitKey(waitTime);  // wait for key to be pressed
    if (key == 's')
        waitTime = waitTime == 0 ? 1 : 0;
}


void OnFrame(SceneData sd, threadsafe_queue<SceneData>& processedFrames)
{
    displayImage(sd);
    switch (ps)
    {
        case GET_NYMBOT_ID_TO_CALIBRATE:
        {
            bool volatile stop = false;
            std::thread t([&stop, &processedFrames]()
            {
                while (!stop)
                {
                    displayImage(*processedFrames.wait_and_pop());
                }
            });
            cout << "Select nymbot ID to calibrate: \nPlace selected nymbot in the center of arena" << endl;
            cin >> in_calibration_nymbot;
            stop = true;
            t.join();
            cout << " Trying to calibrate nymbot " << in_calibration_nymbot << endl;
            ps = ATTEMPT_CALIBRATE_SELECTED_NYMBOT;
            break;
        }
        case SELECT_ENGINE_TO_CALIBRATE:
        {
            nymbot_location_data& current_working_nymbot = nymbots_arr[in_calibration_nymbot];
            current_working_nymbot.delta == 0 ?
                current_working_nymbot.engine_to_fix = NO_ENGINE :
            current_working_nymbot.delta > 0 ?
                current_working_nymbot.engine_to_fix = LEFT_ENGINE :
                current_working_nymbot.engine_to_fix = RIGHT_ENGINE;
            ps = IS_NYMBOT_CALIBRATED;
            break;
        }
        case ATTEMPT_CALIBRATE_SELECTED_NYMBOT:
        {
            Locust& l =  sd.Locusts.at(in_calibration_nymbot);
            nymbot_location_data& current_working_nymbot = nymbots_arr[in_calibration_nymbot];

            if (! current_working_nymbot.handled)
            {
                current_working_nymbot.t = Timer(std::string(to_string(in_calibration_nymbot)));
                current_working_nymbot.t.start();
                current_working_nymbot.initialOrientation = l.GetOrientation();
                current_working_nymbot.handled = true;

                cout << "Working on nymbot " << in_calibration_nymbot << endl;
                if (current_working_nymbot.first_time)
                {
                    current_working_nymbot.l_engine_pwr = FULL_ENGINE_PWR;
                    current_working_nymbot.r_engine_pwr = FULL_ENGINE_PWR;
                }

                nymbot_update_pwm(in_calibration_nymbot,
                                  current_working_nymbot.r_engine_pwr,
                                  current_working_nymbot.l_engine_pwr,
                                  0xFFFF);
            }

            RECORD_TIME(in_calibration_nymbot,t)
            if(current_working_nymbot.t.getAverage(aruco::Timer::SEC) >= TEST_TIME)
            {
                nymbot_update_pwm(in_calibration_nymbot, STOP, 0xFFFF);
                cout << "told nymbot to stop" << endl;
                current_working_nymbot.finalOrientation = l.GetOrientation();
                current_working_nymbot.delta = current_working_nymbot.finalOrientation - current_working_nymbot.initialOrientation;
                if (current_working_nymbot.first_time)
                {
                    ps = SELECT_ENGINE_TO_CALIBRATE;
                    current_working_nymbot.first_time = false;
                }
                else
                {
                    ps = IS_NYMBOT_CALIBRATED;
                }
            }
            break;
        }
        case IS_NYMBOT_CALIBRATED:
        {
            nymbot_location_data& current_working_nymbot = nymbots_arr[in_calibration_nymbot];
            switch (current_working_nymbot.engine_to_fix)
            {
                case ENGINE::LEFT_ENGINE:
                    if (current_working_nymbot.delta > 0)
                    {
                        if (current_working_nymbot.delta >= 0 && current_working_nymbot.delta <= 10)
                        {
                            current_working_nymbot.l_engine_pwr += 1;
                            cout << "Removed one unit of power from left engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 10 && current_working_nymbot.delta <= 20)
                        {
                            current_working_nymbot.l_engine_pwr += 2;
                            cout << "Removed two units of power from left engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 20 && current_working_nymbot.delta <= 30)
                        {
                            current_working_nymbot.l_engine_pwr += 3;
                            cout << "Removed three units of power from left engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 30 && current_working_nymbot.delta <= 40)
                        {
                            current_working_nymbot.l_engine_pwr += 4;
                            cout << "Removed four units of power from left engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 40 && current_working_nymbot.delta <= 50)
                        {
                            current_working_nymbot.l_engine_pwr += 5;
                            cout << "Removed five units of power from left engine" << endl;
                        }
                        else
                        {
                            current_working_nymbot.l_engine_pwr += 7;
                            cout << "Removed seven units of power from left engine" << endl;
                        }
                    }
                    else if (current_working_nymbot.delta == 0)
                    {
                        cout << "No changes needs to be made" << endl;
                    }
                    else
                    {
                        if (current_working_nymbot.delta <= 0 && current_working_nymbot.delta >= -10)
                        {
                            current_working_nymbot.l_engine_pwr -= 1;
                            cout << "Added one unit of power to left engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -10 && current_working_nymbot.delta >= -20)
                        {
                            current_working_nymbot.l_engine_pwr -= 2;
                            cout << "Added two units of power to left engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -20 && current_working_nymbot.delta >= -30)
                        {
                            current_working_nymbot.l_engine_pwr -= 3;
                            cout << "Added three units of power to left engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -30 && current_working_nymbot.delta >= -40)
                        {
                            current_working_nymbot.l_engine_pwr -= 4;
                            cout << "Added four units of power to left engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -40 && current_working_nymbot.delta >= -50)
                        {
                            current_working_nymbot.l_engine_pwr -= 5;
                            cout << "Added five units of power to left engine" << endl;
                        }
                        else
                        {
                            current_working_nymbot.l_engine_pwr -= 7;
                            cout << "Added seven units of power to left engine" << endl;
                        }

                        if (current_working_nymbot.l_engine_pwr < 0)
                        {
                            cout << "Max engine threshold reached setting to " << FULL_ENGINE_PWR << endl;
                            current_working_nymbot.l_engine_pwr = FULL_ENGINE_PWR;
                        }
                    }
                    break;
                case ENGINE::RIGHT_ENGINE:
                    if (current_working_nymbot.delta > 0)
                    {
                        if (current_working_nymbot.delta >= 0 && current_working_nymbot.delta <= 10)
                        {
                            current_working_nymbot.l_engine_pwr -= 1;
                            cout << "Added one unit of power from right engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 10 && current_working_nymbot.delta <= 20)
                        {
                            current_working_nymbot.l_engine_pwr -= 2;
                            cout << "Added two units of power from right engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 20 && current_working_nymbot.delta <= 30)
                        {
                            current_working_nymbot.l_engine_pwr -= 3;
                            cout << "Added three units of power from right engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 30 && current_working_nymbot.delta <= 40)
                        {
                            current_working_nymbot.l_engine_pwr -= 4;
                            cout << "Added four units of power from right engine" << endl;
                        }
                        else if (current_working_nymbot.delta >= 40 && current_working_nymbot.delta <= 50)
                        {
                            current_working_nymbot.l_engine_pwr -= 5;
                            cout << "Added five units of power from right engine" << endl;
                        }
                        else
                        {
                            current_working_nymbot.l_engine_pwr -= 7;
                            cout << "Added seven units of power from right engine" << endl;
                        }
                    }
                    else if (current_working_nymbot.delta == 0)
                    {
                        cout << "No changes needs to be made" << endl;
                    }
                    else // delta is negative
                    {
                        if (current_working_nymbot.delta <= 0 && current_working_nymbot.delta >= -10)
                        {
                            current_working_nymbot.r_engine_pwr += 1;
                            cout << "Removed one unit of power to right engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -10 && current_working_nymbot.delta >= -20)
                        {
                            current_working_nymbot.r_engine_pwr += 2;
                            cout << "Removed two units of power to right engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -20 && current_working_nymbot.delta >= -30)
                        {
                            current_working_nymbot.r_engine_pwr += 3;
                            cout << "Removed three units of power to right engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -30 && current_working_nymbot.delta >= -40)
                        {
                            current_working_nymbot.r_engine_pwr += 4;
                            cout << "Removed four units of power to right engine" << endl;
                        }
                        else if (current_working_nymbot.delta <= -40 && current_working_nymbot.delta >= -50)
                        {
                            current_working_nymbot.r_engine_pwr += 5;
                            cout << "Removed five units of power to right engine" << endl;
                        }
                        else
                        {
                            current_working_nymbot.r_engine_pwr += 7;
                            cout << "Removed seven units of power to right engine" << endl;
                        }
                        if (current_working_nymbot.r_engine_pwr < 0)
                        {
                            cout << "Max engine threshold reached setting to " << FULL_ENGINE_PWR << endl;
                            current_working_nymbot.r_engine_pwr = FULL_ENGINE_PWR;
                        }
                    }
                    break;
                case ENGINE::NO_ENGINE:
                    cout << "No Engine needs to calibrated please continue to next nymbot" << endl;
                    break;
                default:
                    cout << "SOMETHING WENT WRONG PLEASE STOP" << endl;
                    break;
            }
            int option;
            cout << "Select you option:\n1) Continue Working on nymbot " << in_calibration_nymbot << "\n2) Export data to file\n3) Select new nymbot to calibrate" << endl;
            bool volatile stop = false;

            std::thread t([&stop, &processedFrames]()
                          {
                              while (!stop)
                              {
                                  displayImage(*processedFrames.wait_and_pop());
                              }
                          });
            cin >> option;
            stop = true;
            t.join();

            switch (option)
            {
                case 1:
                    ps = ATTEMPT_CALIBRATE_SELECTED_NYMBOT;
                    break;
                case 2:
                    ps = WRITE_DATA_TO_FILE;
                    break;
                case 3:
                    ps = GET_NYMBOT_ID_TO_CALIBRATE;
                    break;
                default:
                    break;
            }

            current_working_nymbot.handled = false;
            current_working_nymbot.t = Timer(to_string(in_calibration_nymbot));
            current_working_nymbot.t.start();

            break;
        }

        case WRITE_DATA_TO_FILE:
        {

            std::fstream calibrationDataFile(CALIBRATION_DATA_FILE_NAME, ios::out | ios::in);

            if (!calibrationDataFile)
            {
                calibrationDataFile.close();
                calibrationDataFile.open(CALIBRATION_DATA_FILE_NAME, ios::out);
                if(!calibrationDataFile)
                {
                    cout << "Failed to open file" << endl;
                    exit(1);
                }
            }
            if (calibrationDataFile.peek() == std::ifstream::traits_type::eof())
            {
                calibrationDataFile.clear();
                calibrationDataFile << "id,pwm1,pwm2,k_l,k_r" << endl;
                for(int i = 0; i < ROBOT_MAX_NUMBER; i++)
                {
                    if (nymbots_arr[i].l_engine_pwr == -1 || nymbots_arr[i].r_engine_pwr == -1)
                    {
                        nymbots_arr[i].l_engine_pwr = FULL_ENGINE_PWR;
                        nymbots_arr[i].r_engine_pwr = FULL_ENGINE_PWR;
                    }
                }
                for (int i = 0; i < sd.Locusts.size(); i++)
                {
                    float k_r = 0, k_l = 0;
                    (100 - nymbots_arr[i].r_engine_pwr) !=  100 ?
                            k_r = (float) nymbots_arr[i].r_engine_pwr / 100 :
                            k_l = (float) nymbots_arr[i].l_engine_pwr / 100;

                    calibrationDataFile << to_string(i) << "," << to_string(100 - nymbots_arr[i].l_engine_pwr)
                            << "," << to_string(100 - nymbots_arr[i].r_engine_pwr) << "," << (to_string((1-k_l))) << "," << (to_string((1-k_r))) << endl;
                }
                calibrationDataFile.close();
                ps = END_PROGRAM;
                break;
            }
            std::fstream temp(TEMP_CALIBRATION_DATA_FILE_NAME, ios::in | ios::out | ios::trunc);
            temp.clear();
            string line, word;
            getline(calibrationDataFile, line);
            temp << line << endl;
            std::vector<string> row;

            while (! getline(calibrationDataFile, line).eof())
            {
                row.clear();

                stringstream s(line);
                while(getline(s,word, ','))
                {
                    row.push_back(word);
                }

                if (nymbots_arr[stoi(row[0])].l_engine_pwr != -1 || nymbots_arr[stoi(row[0])].r_engine_pwr != -1)
                {
                    float k_r = 0, k_l = 0;
                    (100 - nymbots_arr[stoi(row[0])].r_engine_pwr) !=  100 ?
                            k_r = (float) nymbots_arr[stoi(row[0])].r_engine_pwr / 100 :
                            k_l = (float) nymbots_arr[stoi(row[0])].l_engine_pwr / 100;
                    temp <<
                            row[0] << "," << to_string(100 - nymbots_arr[stoi(row[0])].l_engine_pwr)
                            << "," << to_string(100 - nymbots_arr[stoi(row[0])].r_engine_pwr) << ","  <<
                            (to_string((1-k_l))) << "," << (to_string((1-k_r))) << endl;
                }
                else
                {
                    temp << line << endl;
                }
            }
            calibrationDataFile.close();
            remove(CALIBRATION_DATA_FILE_NAME);
            rename(TEMP_CALIBRATION_DATA_FILE_NAME, CALIBRATION_DATA_FILE_NAME);
            temp.close();
        }
        case END_PROGRAM:
        {
            cin.clear();
            cout << "press enter to exit the program" << endl;
            cin.ignore();
            cin.ignore();
            exit(0);
        }
        default:
        {
            break;
        }
    }
}

int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
	string cameraJsonFile;

    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");

    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    nymbot_init("/dev/ttyUSB0");

    AsyncDetector d(configFile, vs);
    d.Start();

    threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames() ;

    while (d.IsProcessing()) {
        std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();
        OnFrame(*frame, ProcessedFrames);
    }

    return 0;
}
