#include <iostream>
#include <fstream>
#include <timers.h>
#include <string>
extern "C" {
#include <nymbot_transmitter_low.h>
}
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"

#define TESTED_ID 12
#define FULL_ENGINE_PWR 0
#define STOP 100, 100
#define RECORD_TIME(id, type) nymbots_arr[id].type.end();

typedef enum
{
    NO_ENGINE,
    LEFT_ENGINE,
    RIGHT_ENGINE
} ENGINE;

static struct nymbot_location_data {
    cv::Point initialPos;
    int initialOrientation, finalOrientation, delta;
    int l_engine_pwr = FULL_ENGINE_PWR, r_engine_pwr = FULL_ENGINE_PWR;
    bool handled = false;
    ENGINE engine_to_fix;
    Timer t;
    bool first_time = true;
} nymbots_arr[ROBOT_MAX_NUMBER];

static uint8_t current_nymbot = 0;

void OnFrame(SceneData sd, threadsafe_queue<SceneData>& processedFrames) {
    vector<cv::Point> pastPos;
//    cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
    cv::Mat img = sd.LastFrame.clone();
    for (auto& l : sd.Locusts)
    {
        if (l.m_Marker.id != current_nymbot || !l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
        {
            continue;
        }
        l.m_Marker.draw(img);
        int id = l.m_Marker.id;
        cv::Point p1(l.GetPositionIJ());
        pastPos.push_back(p1);
        int orientation = l.GetOrientation();
        //cv::Point2d xy = sd.GetPositionXY(l);
        if (!nymbots_arr[id].handled) {
            nymbots_arr[id].t = Timer(std::string(to_string(id)));
            nymbots_arr[id].t.start();
            cout << "Working on nymbot " << id << endl;
            nymbot_update_pwm(id, nymbots_arr[id].r_engine_pwr, nymbots_arr[id].l_engine_pwr, 0xFFFF);
            nymbots_arr[id].initialOrientation = l.GetOrientation();
            nymbots_arr[id].initialPos = cv::Point(l.GetPositionIJ());
            nymbots_arr[id].handled = true;
        }
        RECORD_TIME(id,t)
        if(nymbots_arr[id].t.getAverage(aruco::Timer::SEC) >= 4) {
            volatile bool stop = false;
            nymbot_update_pwm(id, STOP, 0xFFFF);
            nymbots_arr[id].finalOrientation = l.GetOrientation();
            nymbots_arr[id].delta =  nymbots_arr[id].finalOrientation - nymbots_arr[id].initialOrientation;
            if(nymbots_arr[id].first_time)
            {
                nymbots_arr[id].first_time = false;
                if (nymbots_arr[id].delta > 4) // Work on left engine
                {
                    cout << "\nWorking on left engine" << endl;
                    nymbots_arr[id].engine_to_fix = ENGINE::LEFT_ENGINE;
                }
                else if (nymbots_arr[id].delta <= 4 && nymbots_arr[id].delta >= -4) // No changes needed
                {
                    cout << "\nCalibration finished" << endl;
                    nymbots_arr[id].engine_to_fix = ENGINE::NO_ENGINE;
                }
                else // Work on right engine
                {
                    cout << "\nWorking on right engine" << endl;
                    nymbots_arr[id].engine_to_fix = ENGINE::RIGHT_ENGINE;
                }
            }
            switch (nymbots_arr[id].engine_to_fix)
            {
                case ENGINE::LEFT_ENGINE:
                    if (nymbots_arr[id].delta > 0)
                    {
                        if (nymbots_arr[id].delta >= 0 && nymbots_arr[id].delta <= 10)
                        {
                            nymbots_arr[id].l_engine_pwr += 1;
                            cout << "Removed one unit of power from left engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 10 && nymbots_arr[id].delta <= 20)
                        {
                            nymbots_arr[id].l_engine_pwr += 2;
                            cout << "Removed two units of power from left engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 20 && nymbots_arr[id].delta <= 30)
                        {
                            nymbots_arr[id].l_engine_pwr += 3;
                            cout << "Removed three units of power from left engine" << endl;
                            }
                        else if (nymbots_arr[id].delta >= 30 && nymbots_arr[id].delta <= 40)
                        {
                            nymbots_arr[id].l_engine_pwr += 4;
                            cout << "Removed four units of power from left engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 40 && nymbots_arr[id].delta <= 50)
                        {
                            nymbots_arr[id].l_engine_pwr += 5;
                            cout << "Removed five units of power from left engine" << endl;
                        }
                        else
                        {
                            nymbots_arr[id].l_engine_pwr += 7;
                            cout << "Removed seven units of power from left engine" << endl;
                        }
                    }
                    else if (nymbots_arr[id].delta == 0)
                    {
                        cout << "No changes needs to be made" << endl;
                    }
                    else {
                        if (nymbots_arr[id].delta <= 0 && nymbots_arr[id].delta >= -10) {
                            nymbots_arr[id].l_engine_pwr -= 1;
                            cout << "Added one unit of power to left engine" << endl;
                        } else if (nymbots_arr[id].delta <= -10 && nymbots_arr[id].delta >= -20) {
                            nymbots_arr[id].l_engine_pwr -= 2;
                            cout << "Added two units of power to left engine" << endl;
                        } else if (nymbots_arr[id].delta <= -20 && nymbots_arr[id].delta >= -30) {
                            nymbots_arr[id].l_engine_pwr -= 3;
                            cout << "Added three units of power to left engine" << endl;
                        } else if (nymbots_arr[id].delta <= -30 && nymbots_arr[id].delta >= -40) {
                            nymbots_arr[id].l_engine_pwr -= 4;
                            cout << "Added four units of power to left engine" << endl;
                        } else if (nymbots_arr[id].delta <= -40 && nymbots_arr[id].delta >= -50) {
                            nymbots_arr[id].l_engine_pwr -= 5;
                            cout << "Added five units of power to left engine" << endl;
                        } else {
                            nymbots_arr[id].l_engine_pwr -= 7;
                            cout << "Added seven units of power to left engine" << endl;
                        }

                        if (nymbots_arr[id].l_engine_pwr < 0)
                        {
                            cout << "Max engine threshold reached setting to " << FULL_ENGINE_PWR << endl;
                            nymbots_arr[id].l_engine_pwr = FULL_ENGINE_PWR;
                        }
                    }
                    break;
                case ENGINE::RIGHT_ENGINE:
                    if (nymbots_arr[id].delta > 0)
                    {
                        if (nymbots_arr[id].delta >= 0 && nymbots_arr[id].delta <= 10)
                        {
                            nymbots_arr[id].l_engine_pwr += 1;
                            cout << "Removed one unit of power from right engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 10 && nymbots_arr[id].delta <= 20)
                        {
                            nymbots_arr[id].l_engine_pwr += 2;
                            cout << "Removed two units of power from right engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 20 && nymbots_arr[id].delta <= 30)
                        {
                            nymbots_arr[id].l_engine_pwr += 3;
                            cout << "Removed three units of power from right engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 30 && nymbots_arr[id].delta <= 40)
                        {
                            nymbots_arr[id].l_engine_pwr += 4;
                            cout << "Removed four units of power from right engine" << endl;
                        }
                        else if (nymbots_arr[id].delta >= 40 && nymbots_arr[id].delta <= 50)
                        {
                            nymbots_arr[id].l_engine_pwr += 5;
                            cout << "Removed five units of power from right engine" << endl;
                        }
                        else
                        {
                            nymbots_arr[id].l_engine_pwr += 7;
                            cout << "Removed seven units of power from right engine" << endl;
                        }
                    }
                    else if (nymbots_arr[id].delta == 0)
                    {
                        cout << "No changes needs to be made" << endl;
                    }
                    else // delta is negative
                    {
                        if (nymbots_arr[id].delta <= 0 && nymbots_arr[id].delta >= -10) {
                            nymbots_arr[id].r_engine_pwr -= 1;
                            cout << "Added one unit of power to right engine" << endl;
                        } else if (nymbots_arr[id].delta <= -10 && nymbots_arr[id].delta >= -20) {
                            nymbots_arr[id].r_engine_pwr -= 2;
                            cout << "Added two units of power to right engine" << endl;
                        } else if (nymbots_arr[id].delta <= -20 && nymbots_arr[id].delta >= -30) {
                            nymbots_arr[id].r_engine_pwr -= 3;
                            cout << "Added three units of power to right engine" << endl;
                        } else if (nymbots_arr[id].delta <= -30 && nymbots_arr[id].delta >= -40) {
                            nymbots_arr[id].r_engine_pwr -= 4;
                            cout << "Added four units of power to right engine" << endl;
                        } else if (nymbots_arr[id].delta <= -40 && nymbots_arr[id].delta >= -50) {
                            nymbots_arr[id].r_engine_pwr -= 5;
                            cout << "Added five units of power to right engine" << endl;
                        } else {
                            nymbots_arr[id].r_engine_pwr -= 7;
                            cout << "Added seven units of power to right engine" << endl;
                        }

                        if (nymbots_arr[id].r_engine_pwr < 0)
                        {
                            cout << "Max engine threshold reached setting to " << FULL_ENGINE_PWR << endl;
                            nymbots_arr[id].r_engine_pwr = FULL_ENGINE_PWR;
                        }
                    }
                    break;
                case ENGINE::NO_ENGINE:
                    cout << "No Engine needs to calibrated please continue to next nymbot" << endl;
                    break;
                default:
                    cout << "SOMETHING WENT WRONG PLEASE STOP" << endl;
                    break;
            }

            cout << "\nInitial Orientation " << nymbots_arr[id].initialOrientation << endl;
            cout << "Final Orientation     " << nymbots_arr[id].finalOrientation << endl;
            cout << "Orientation Delta     " << nymbots_arr[id].delta << endl;
            cout << "Left Engine power     " << nymbots_arr[id].l_engine_pwr << endl;
            cout << "Right Engine power    " << nymbots_arr[id].r_engine_pwr << endl;
            cout << "\n*****************\n" << endl;
            cout << "Place robot in the center of arena to continue calibration" << endl;
            cout << "do you want to continue calibration for nymbot "  << id << "? (Y/N) " << endl;

            std::thread t([&stop, &processedFrames] () {
                while (!stop)
                {
                    processedFrames.wait_and_pop();
                }
            });
            uint8_t reply;
            cin >> reply;

            stop = true;
            t.join();

            nymbots_arr[id].handled = false;
            nymbots_arr[id].t = Timer(to_string(id));
            nymbots_arr[id].t.start();


            if (reply == 'y' || reply == 'Y')
            {

            }
            else if( reply == 'n' || reply == 'N')
            {
                if (id == sd.Locusts.size() - 1)
                {
                    ofstream outfile;
                    outfile.open("test");
                    outfile << "id,pwm1,pwm2,k\n";
                    cout << "Finished calibrating exporting all data to file" << endl;

                    for (int i = 0; i < sd.Locusts.size(); i++)
                    {
                        float k;
                        (100 - nymbots_arr[i].r_engine_pwr) !=  100 ?
                        k = (float) nymbots_arr[i].r_engine_pwr / 100 :
                        k = (float) nymbots_arr[i].l_engine_pwr / 100;
                        outfile << to_string(i) << "," << to_string(100 - nymbots_arr[i].l_engine_pwr)
                                << "," << to_string(100 - nymbots_arr[i].r_engine_pwr) << "," << (to_string((1-k))) << endl;
                    }
                    outfile.close();
                    cout << "press enter to exit the program" << endl;
                    cin.ignore();
                    exit(0);
                }
                cout << "moving on to the next nymbot" << endl;
                if (current_nymbot == 3)
                    current_nymbot += 1;
                current_nymbot++;
            }
            else
            {
                cout << "Wrong option" << endl;
            }
        }
        for(auto& p : pastPos){
            cv::putText(img, //target image
                        cv::format("*"), //text
                        p, //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        1.0,
                        CV_RGB(118, 185, 0), //font color
                        2);
        }
    }
    //cv::Mat map = sd.GetMap();

    cv::resize(img, img, cv::Size(1200, 600));
    cv::imshow("image", img);
    int waitTime = 1;
    char key = 0;
    key = cv::waitKey(waitTime);  // wait for key to be pressed
    if (key == 's')
        waitTime = waitTime == 0 ? 1 : 0;
}

int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
	string cameraJsonFile;

    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");

    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    nymbot_init("/dev/ttyUSB0");
    cout << "Welcome to nymbot calibration program, please pay attention to the following instructions:" <<endl;
    cout << "Place robot id: 0 in the middle of the arena with orientation facing 180 +- 10" << endl;
    cout << "Once ready press ENTER" << endl;

    cin.ignore();

    AsyncDetector d(configFile, vs);
    d.Start();

    threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames() ;

    while (d.IsProcessing()) {
        std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();
        OnFrame(*frame, ProcessedFrames);
    }

    return 0;
}
