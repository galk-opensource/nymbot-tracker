#include <iostream>
#include <unistd.h>
#include <fstream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"
extern "C" {
#include <nymbot_transmitter.h>
}
static struct nymbot_location_data
{
    cv::Point initialPos ={-1, -1};
    cv::Point currentPosition;
    int barcode_id = -1;
} nymbots_arr[ROBOT_MAX_NUMBER];
static volatile bool first_run = true;
static string save_path= "./barcode_to_id_map.txt";

static int RegisterLocation(SceneData& sd)
{
    int nymbot_id;

    if (first_run)
    {
        cout << "First Run\n" << endl;
        first_run = false;
        for(Locust& l : sd.Locusts)
        {
            if(!l.m_Marker.isValid())
            {
                continue;
            }
            nymbot_id = l.m_Marker.id;
            nymbots_arr[nymbot_id].initialPos = cv::Point (l.GetPositionIJ());
        }
    }
    else
    {
        cout << "Second Run\n" << endl;
        auto& locusts_vec = sd.Locusts;
        for(Locust& l : locusts_vec)
        {
            if (!l.m_Marker.isValid() ||  nymbots_arr[l.m_Marker.id].initialPos == cv::Point(-1, -1))
            {
                continue;
            }
            nymbot_id = l.m_Marker.id;
            nymbots_arr[nymbot_id].currentPosition = cv::Point (l.GetPositionIJ());
            if ( 5 < cv::norm(nymbots_arr[nymbot_id].initialPos - nymbots_arr[nymbot_id].currentPosition))
            {
                nymbots_arr[nymbot_id].initialPos = nymbots_arr[nymbot_id].currentPosition;
                cout << "Initial position for nymbotID: " << nymbot_id << " Was changed and is now " << nymbots_arr[nymbot_id].initialPos << endl;
                return nymbot_id;
            }
        }
        return -1;
    }
}

static void displayImage(SceneData& sd)
{
    vector<cv::Point> pastPos;
    cv::Mat img = sd.LastFrame.clone();
    for (auto& l : sd.Locusts)
    {
        if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
            continue;
        l.m_Marker.draw(img);
        cv::Point p1(l.GetPositionIJ());
        pastPos.push_back(p1);

    }
    for(auto& p : pastPos){
        cv::putText(img, //target image
                    cv::format("*"), //text
                    p, //top-left position
                    cv::FONT_HERSHEY_DUPLEX,
                    1.0,
                    CV_RGB(118, 185, 0), //font color
                    2);
    }
    cv::resize(img, img, cv::Size(1200, 600));
    cv::imshow("image", img);
    int waitTime = 1;
    char key = 0;
    key = cv::waitKey(waitTime);  // wait for key to be pressed
    if (key == 's')
        waitTime = waitTime == 0 ? 1 : 0;
}


static int nymbot_bluetooth_to_map = 0;

void OnFrame(SceneData sd, threadsafe_queue<SceneData>& processedFrames) {

    cout << "Select ID to map or '-1' to exit" << endl;

    bool volatile stop = false;
    std::thread t1([&stop, &processedFrames]()
                   {
                       while (!stop)
                       {
                           displayImage(*processedFrames.wait_and_pop());
                       }
                   });

    cin >> nymbot_bluetooth_to_map;

    stop = true;
    t1.join();
    stop = false;

    if(-1 == nymbot_bluetooth_to_map)
    {
        std::ofstream map_file (save_path, std::ofstream::out);
        map_file << "nymbot_id" << "," << "Barcode_id" << endl;
        for(int i = 0; i < ROBOT_MAX_NUMBER; i++)
        {
            map_file << i << "," << nymbots_arr[i].barcode_id << endl;
        }
        cout<< "saved the map file under " << save_path <<endl;
        map_file.close();
        exit(0);
    }

    nymbot_update_pwm(nymbot_bluetooth_to_map, 0, 0, 0xFFFF);

    std::thread t2([&stop, &processedFrames]()
                   {
                       while (!stop)
                       {
                           displayImage(*processedFrames.wait_and_pop());
                       }
                   });
    sleep(3);

    nymbot_update_pwm(nymbot_bluetooth_to_map, 100, 100, 0xFFFF);
    sleep(2);
    stop = true;
    t2.join();
    stop = false;

    int bardcode_id = RegisterLocation(*processedFrames.wait_and_pop());

    first_run = true;
    if(bardcode_id != -1)
    {
        nymbots_arr[nymbot_bluetooth_to_map].barcode_id = bardcode_id;
        cout << nymbot_bluetooth_to_map << " Was mapped to " << bardcode_id << endl;
    }
    else
    {
        cout << "Could not map: " << nymbot_bluetooth_to_map << " Please try again and make sure the barcode are visible to the camera\n" << endl;
    }
}

int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
	string cameraJsonFile;

    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-cameraJson cameraJsonFile.json] [-savePath map.txt]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");
    if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");
    if (cml["-savePath"])
        save_path = cml("-savePath");
    else{
        cout << "Didn't specifiy -savePath. defaulting to save in ""./barcode_to_id_map.txt""" << endl;
    }

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");

    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    nymbot_init("/dev/ttyUSB0");
    nymbot_calibrate("");
    AsyncDetector d(configFile, vs);
    d.Start();

    threadsafe_queue<SceneData>& ProcessedFrames = d.GetProcessedFrames();

    std::shared_ptr< SceneData> frame = ProcessedFrames.wait_and_pop();

    while (d.IsProcessing()) {
        frame = ProcessedFrames.wait_and_pop();
        RegisterLocation(*frame);
        OnFrame(*frame, ProcessedFrames);
    }

    return 0;
}
