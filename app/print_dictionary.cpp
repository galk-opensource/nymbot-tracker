﻿#include "CmdLineParser.h"
#include <fstream>
#include "aruco.h"
#include "opencv2/opencv.hpp"
using namespace aruco;
using namespace cv;


#define A4_SIZE  Point2d(29.7,21.0)
#define A4_300DPI Point2i(3508,2480)
#define A4_600DPI (Point2i(A4_300DPI) * 2)
#define SIZE_MARKER (A4_600DPI.x / A4_SIZE.x)
int main(int argc, char** argv)
{
	CmdLineParser cml(argc, argv);
	string configFile;

	if (argc < 3 || cml["-h"])
	{
		cerr << "Usage: dictionary_file_path output_image_path  marker_size(cm) " << std::endl;
		cout << "Example: Aruco4X4_250.dict Aruco4X4_250.png 0.8" << endl << endl;
		return 0;
	}
	string dictPath = argv[1];
	string outImg = argv[2];
	double markerSizeCM = stod(argv[3]);

	Dictionary dict = Dictionary::loadFromFile(dictPath);

	int markerSizePX(SIZE_MARKER * (markerSizeCM + 0.3));

	Mat a4 = Mat::ones(A4_600DPI.x, A4_600DPI.y, CV_8UC1) * 255;
	int maxInRow = int(A4_600DPI.y / (markerSizePX * 2)) -1;
	int	numCols = cv::min(maxInRow, int(dict.size()));
	int	numRows = int((int(dict.size()) / maxInRow)) + 1;
	cout<< "print " << dict.size()<< " markers.." << endl;
	for (size_t i = 0; i < numRows; i++)
		for (size_t j = 0; j < numCols; j++)
		{
			int index = i * numCols + j;
			if (index >= dict.size())
				break;
			Mat markerImg = dict.getMarkerImage_id(index, dict.nbits()+2);
			cv::resize(markerImg, markerImg, Size(markerSizePX, markerSizePX));
			markerImg.copyTo(a4(Range(markerSizePX+ i * markerSizePX * 2, markerSizePX+ i * markerSizePX * 2 + markerSizePX), Range(markerSizePX+ j * markerSizePX * 2, markerSizePX+ j * markerSizePX * 2 + markerSizePX)));
		} 
	cv::imwrite(outImg, a4);
	cout << "dictionary writed to " << outImg;
	return 0;
}