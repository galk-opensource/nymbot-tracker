#include <iostream>
#include "Detector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"

// The barcodes index in the sorted  coreners barcode vector 
#define TOP_LEFT_BARCODE 0
#define TOP_RIGHT_BARCODE 1
#define BOTTOM_LEFT_BARCODE 2
#define BOTTOM_RIGHT_BARCODE 3
// The arena lines indexes
#define TOP_LINE 0
#define RIGHT_LINE 1
#define BOTTOM_LINE 2
#define LEFT_LINE 3
// key to click for saving arena config
#define SAVE_CONFIG_KEY 's'
// distance error for barcodes on the same line. If error is bigger then value,line is drawn as red.
#define BARCODE_LINE_DISTANCE_ERROR_PIXELS 20
// orientation error for the barcodes themself
#define BARCODE_ORIENTATION_ERROR 5
// opencv colorss consts
const cv::Scalar RED = cv::Scalar(0,0,255);
const cv::Scalar BLACK = cv::Scalar(0,0,0);
const cv::Scalar GREEN = cv::Scalar(0,255,0);

using namespace cv;

static string arena_Config_File;

// https://answers.opencv.org/question/9511/how-to-find-the-intersection-point-of-two-lines/
// Finds the intersection of two lines, or returns false.
// The lines are defined by (o1, p1) and (o2, p2).
bool intersection(Point o1, Point p1, Point o2, Point p2,
                      Point &r)
{
    Point x = o2 - o1;
    Point d1 = p1 - o1;
    Point d2 = p2 - o2;

    float cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < /*EPS*/1e-8)
        return false;

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    r = o1 + d1 * t1;
    return true;
}

void buildConfig(SceneData sd,vector<Point> sortedBarcodes,vector<int> arenaLines,int barcodeSizemm,int topBorderSizemm,int rightBorderSizemm,int bottomBorderSizemm,int leftBorderSizemm,Point middlePoint){
	FileStorage fs;
	fs.open(arena_Config_File,FileStorage::WRITE);
	fs.writeComment("Barcodes x and y coordinates (center of barcodes");
	fs << "top-left" << sortedBarcodes[TOP_LEFT_BARCODE];
	fs << "top-right" << sortedBarcodes[TOP_RIGHT_BARCODE];
	fs << "bottom-left" << sortedBarcodes[BOTTOM_LEFT_BARCODE];
	fs << "bottom-right" << sortedBarcodes[BOTTOM_RIGHT_BARCODE];
	fs.writeComment("Middle point of the arena");
	fs << "middle-arena" << middlePoint;
	fs.writeComment("Arena borders pixel size (center to center of barcodes)");
	fs << "top-border-size-pixels" << arenaLines[TOP_LINE];
	fs << "right-border-size-pixels" << arenaLines[RIGHT_LINE];
	fs << "bottom-border-size-pixels" << arenaLines[BOTTOM_LINE];
	fs << "left-border-size-pixels" << arenaLines[LEFT_LINE];
	fs.writeComment("barcode size in mm");
	fs << "barcodes-size-mm" << barcodeSizemm;
	fs.writeComment("Arena borders in mm(center to center of barcodes)");
	fs << "top-border-size-mm" << topBorderSizemm + barcodeSizemm;
	fs << "right-border-size-mm" << rightBorderSizemm + barcodeSizemm;
	fs << "bottom-border-size-mm" << bottomBorderSizemm + barcodeSizemm;
	fs << "left-border-size-mm" << leftBorderSizemm + barcodeSizemm;
	fs.release();
}

// In case there are no 4 barcodes in the arena,arena_calibration cant be made. calling other onFrame prevents segmetion fault.
void OnFrameFallback(SceneData sd) {
	cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
	cv::Mat img = sd.LastFrame.clone();
	for (auto& l : sd.Locusts)
	{
		if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
			continue;
		l.m_Marker.draw(img);
		int id = l.m_Marker.id;
		cv::Point p(l.GetPositionIJ());
		int orientation = l.GetOrientation();
		//cv::Point2d xy = sd.GetPositionXY(l);
		cv::putText(img, //target image
			cv::format("%d,(%d,%d),%d", id, p.x, p.y, orientation), //text
			p, //top-left position
			cv::FONT_HERSHEY_DUPLEX,
			1.0,
			CV_RGB(118, 185, 0), //font color
			2);
	}
	cv::Mat map = sd.GetMap();

	cv::resize(img, img, cv::Size(1200, 600));
	cv::imshow("image", img);
	int waitTime = 10;
	char key = 0;
	key = cv::waitKey(waitTime);  // wait for key to be pressed
	if (key == 's')
		waitTime = waitTime == 0 ? 10 : 0;
}

/*
* Calculate which barcode is in which corner.
* because we don't know the relative placement of each barcode to the others,we must check their x and y values and decide
* based on number of other barcodes who have values bigger or smaller than them where each barcode placement is
*/ 
vector<Point> barcodesCornerSort(vector<Point> barcodesVector){
	vector<Point> sortedBarcodes = barcodesVector;
	for(int i=0;i<4;i++){
		// save for each barcode how many other barcodes have higher values then him
		int xValue =0;
		int yValue=0;
		for(int j=0;j<4 ;j++){
			if(i==j){continue;}

			if (barcodesVector[i].x <= barcodesVector[j].x){
				xValue+=1;
			}
			if (barcodesVector[i].y <= barcodesVector[j].y){
				yValue+=1;
			}

		}
		// Decide on which corner the barcode is
		if ((xValue == 2 || xValue == 3) && (yValue == 2 || yValue == 3)){
			sortedBarcodes[TOP_LEFT_BARCODE] = barcodesVector[i];
		} 
		else if((xValue == 0 || xValue == 1) && (yValue == 2 || yValue == 3)){
			sortedBarcodes[TOP_RIGHT_BARCODE] = barcodesVector[i];
		}
		else if((xValue == 2 || xValue == 3) && (yValue == 0 || yValue == 1)){
			sortedBarcodes[BOTTOM_LEFT_BARCODE] = barcodesVector[i];
		} 
		else if((xValue == 0 || xValue == 1) && (yValue == 0 || yValue == 1)){
			sortedBarcodes[BOTTOM_RIGHT_BARCODE] = barcodesVector[i];
		} 
	}
	return sortedBarcodes;
}

///// color lines by distance from other barcodes ////
void OnFrame(SceneData sd) {
	// If count of markers isn't 4,onFrame to deafult behavior of just showing the barcodes
	if (sd.MarkerDetected !=4){
		OnFrameFallback(sd);
		return;
	}
	vector<Point> arenaPoints;
	vector<int> arenaBordersSize(4);

	cout << "onFrame:" << sd.FrameNo << "\t" << sd.MarkerDetected << endl;
	cv::Mat img = sd.LastFrame.clone();
	for (auto& l : sd.Locusts)
	{
		if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != sd.FrameNo)
			continue;
		int orientation = l.GetOrientation();
		// check with error of 5 degrees if the barcode orientation is 0.
		// if not in orientation - color RED
		if (orientation < 360-BARCODE_ORIENTATION_ERROR && orientation > BARCODE_ORIENTATION_ERROR) {
			l.m_Marker.draw(img,RED,5,false);
		}
		// if in orientation - color GREEN
		else{
			l.m_Marker.draw(img,GREEN,5);
		}
		Point p(l.GetPositionIJ());
		arenaPoints.push_back(p);
		
	}

	// calculate barcodes corners placements
	vector<Point> sortedCornersBarcodes = barcodesCornerSort(arenaPoints);

	
	// Draw lines
	for(int i=0;i<4;i++){
		for(int j=i+1;j<4;j++){
			if(i==j){continue;}
			
			// Calculate line length and middle point
			int lineLength = norm(sortedCornersBarcodes[i]-sortedCornersBarcodes[j]);
			Point lineMiddlePoint = (sortedCornersBarcodes[i]+sortedCornersBarcodes[j])*0.5;
			// Save line length if its a border to the correspanding placement in the arenaBordersSize vector.
			// Also, check if barcodes are on the same line. If they are, color line green. else,color red.
			if( i == TOP_LEFT_BARCODE && j == TOP_RIGHT_BARCODE){
				int topLeftY = sortedCornersBarcodes[i].y;
				int topRightY = sortedCornersBarcodes[j].y;
				// Test if distance error between barcodes is smaller than 'BARCODE_LINE_DISTANCE_ERROR_PIXELS' value
				if(topLeftY >= topRightY && (topRightY + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > topLeftY ||
					topRightY >= topLeftY && (topLeftY + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > topRightY){
						// If it is, draw green
						cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],GREEN,20);
					}
				else{
					// draw red
					cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],RED,20);
				}
				// Save border size
				arenaBordersSize[TOP_LINE] = lineLength;
				int axisPixelDifference = abs(sortedCornersBarcodes[i].y-sortedCornersBarcodes[j].y);
				// Print to screen line length and the axis pixels difference between barcodes
				putText(img,format("Top Line length: %d pixels",lineLength),lineMiddlePoint+ Point(-300,150),FONT_HERSHEY_DUPLEX,2,BLACK,5);
				putText(img,format("Axis pixels difference: %d pixels",axisPixelDifference),lineMiddlePoint+ Point(-300,250),FONT_HERSHEY_DUPLEX,2,BLACK,5);
			}
			else if( i == TOP_LEFT_BARCODE && j == BOTTOM_LEFT_BARCODE){
				 int topLeftX = sortedCornersBarcodes[i].x;
				int bottomLeftX = sortedCornersBarcodes[j].x;
				// Test if distance error between barcodes is smaller than 'BARCODE_LINE_DISTANCE_ERROR_PIXELS' value
				if(topLeftX >= bottomLeftX && (bottomLeftX + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > topLeftX ||
					bottomLeftX >= topLeftX && (topLeftX + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > bottomLeftX){
						// If it is, draw green
						cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],GREEN,20);
					}
				else{
					// draw red
					cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],RED,20);
				}
				// Save border size
				arenaBordersSize[LEFT_LINE] = lineLength;
				int axisPixelDifference = abs(sortedCornersBarcodes[i].x-sortedCornersBarcodes[j].x);
				// Print to screen line length and the axis pixels difference between barcodes
				putText(img,format("Left Line length: %d pixels",lineLength),lineMiddlePoint+ Point(0,150),FONT_HERSHEY_DUPLEX,2,BLACK,5);
				putText(img,format("Axis pixels difference: %d pixels",axisPixelDifference),lineMiddlePoint+ Point(0,250),FONT_HERSHEY_DUPLEX,2,BLACK,5);
				}
			else if( i == TOP_RIGHT_BARCODE && j == BOTTOM_RIGHT_BARCODE){
				 int topRightX = sortedCornersBarcodes[i].x;
				int bottomRightX = sortedCornersBarcodes[j].x;
				// Test if distance error between barcodes is smaller than 'BARCODE_LINE_DISTANCE_ERROR_PIXELS' value
				if(topRightX >= bottomRightX && (bottomRightX + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > topRightX ||
					bottomRightX >= topRightX && (topRightX + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > bottomRightX){
						// If it is, draw green
						cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],GREEN,20);
					}
				else{
					// draw red
					cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],RED,20);
				}
				// Save border size
				arenaBordersSize[RIGHT_LINE] = lineLength;
				int axisPixelDifference = abs(sortedCornersBarcodes[i].x-sortedCornersBarcodes[j].x);
				// Print to screen line length and the axis pixels difference between barcodes
				putText(img,format("Right Line length: %d pixels",lineLength),lineMiddlePoint+ Point(-600,150),FONT_HERSHEY_DUPLEX,2,BLACK,5);
				putText(img,format("Axis pixels difference: %d pixels",axisPixelDifference),lineMiddlePoint+ Point(-600,250),FONT_HERSHEY_DUPLEX,2,BLACK,5);

				 }
			else if( i == BOTTOM_LEFT_BARCODE && j == BOTTOM_RIGHT_BARCODE){
				 int bottomLeftY = sortedCornersBarcodes[i].y;
				int bottomRightY = sortedCornersBarcodes[j].y;
				// Test if distance error between barcodes is smaller than 'BARCODE_LINE_DISTANCE_ERROR_PIXELS' value
				if(bottomLeftY >= bottomRightY && (bottomRightY + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > bottomLeftY ||
					bottomRightY >= bottomLeftY && (bottomLeftY + BARCODE_LINE_DISTANCE_ERROR_PIXELS) > bottomRightY){
						// If it is, draw green
						cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],GREEN,20);
					}
				else{
					// draw red
					cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],RED,20);
				}
				// Save border size
				arenaBordersSize[BOTTOM_LINE] = lineLength;
				int axisPixelDifference = abs(sortedCornersBarcodes[i].y-sortedCornersBarcodes[j].y);
				// Print to screen line length and the axis pixels difference between barcodes
				putText(img,format("Bottom Line length: %d pixels",lineLength),lineMiddlePoint+ Point(-300,-250),FONT_HERSHEY_DUPLEX,2,BLACK,5);
				putText(img,format("Axis pixels difference: %d pixels",axisPixelDifference),lineMiddlePoint+ Point(-300,-150),FONT_HERSHEY_DUPLEX,2,BLACK,5);

				 }
			else {cv::line(img,sortedCornersBarcodes[i],sortedCornersBarcodes[j],(0,0,0),10);}
			
		}
	}


	// draw corner tags for each barcode
	putText(img,format("TOP-LEFT (%d,%d)",sortedCornersBarcodes[TOP_LEFT_BARCODE].x,sortedCornersBarcodes[TOP_LEFT_BARCODE].y)
	,sortedCornersBarcodes[TOP_LEFT_BARCODE],FONT_HERSHEY_DUPLEX,4,BLACK,3);
	putText(img,format("TOP-RIGHT (%d,%d)",sortedCornersBarcodes[TOP_RIGHT_BARCODE].x,sortedCornersBarcodes[TOP_RIGHT_BARCODE].y)
	,sortedCornersBarcodes[TOP_RIGHT_BARCODE],FONT_HERSHEY_DUPLEX,4,BLACK,3);
	putText(img,format("BOTTOM-LEFT (%d,%d)",sortedCornersBarcodes[BOTTOM_LEFT_BARCODE].x,sortedCornersBarcodes[BOTTOM_LEFT_BARCODE].y)
	,sortedCornersBarcodes[BOTTOM_LEFT_BARCODE],FONT_HERSHEY_DUPLEX,4,BLACK,3);
	putText(img,format("BOTTOM-RIGHT (%d,%d)",sortedCornersBarcodes[BOTTOM_RIGHT_BARCODE].x,sortedCornersBarcodes[BOTTOM_RIGHT_BARCODE].y)
	,sortedCornersBarcodes[BOTTOM_RIGHT_BARCODE],FONT_HERSHEY_DUPLEX,4,BLACK,3);

	// calculate center of arena (bisect of diagonals)
	
	Point middlePoint;
	intersection(sortedCornersBarcodes[TOP_LEFT_BARCODE],sortedCornersBarcodes[BOTTOM_RIGHT_BARCODE],sortedCornersBarcodes[TOP_RIGHT_BARCODE],sortedCornersBarcodes[BOTTOM_LEFT_BARCODE],middlePoint);
	circle(img,middlePoint,50,GREEN,FILLED);
	

	cv::Mat map = sd.GetMap();

	cv::resize(img, img, cv::Size(1200, 600));
	cv::imshow("image", img);
	int waitTime = 10;
	char key = 0;
	key = cv::waitKey(waitTime);  // wait for key to be pressed
	if (key == SAVE_CONFIG_KEY){
		int barcodeSizemm;
		int topBorderSizemm;
		int rightBorderSizemm;
		int bottomBorderSizemm;
		int leftBorderSizemm;
		waitTime = waitTime == 0 ? 10 : 0;

		cout<< "In the following steps you will use a meter to measure The barcode and arena borders sizes in mm (1 meter is 1000 milimeter)" << endl;
		cout<< "Enter the arena barcodes size in millimeter(from black to black),as shown in doc/arena_calibration.md" << endl;
		scanf ("%d",&barcodeSizemm); // TODO: fix to cin
		cout<< "Now we will start the borders measuring part. see doc/arena_calibration.md for pointers" << endl;
		cout << "Measure the top arena border with meter and insert size in mm: " ;
		scanf ("%d",&topBorderSizemm); // TODO: Fix to cin
		cout << "Measure the right arena border with meter and insert size in mm";
		scanf ("%d",&rightBorderSizemm);  // TODO: Fix to cin
		printf("Measure the bottom arena border with meter and insert size in mm\n"); // TODO: fix to cout
		scanf ("%d",&bottomBorderSizemm);  // TODO: Fix to cin
		printf("Measure the left arena border with meter and insert size in mm\n");  // TODO: fix to cout
 		scanf ("%d",&leftBorderSizemm);  // TODO: Fix to cin
		cout<< "Saving config file"<<endl;
		buildConfig(sd,sortedCornersBarcodes,arenaBordersSize,barcodeSizemm,topBorderSizemm,rightBorderSizemm,bottomBorderSizemm,leftBorderSizemm,middlePoint);
		cout << "Built config file\n" << endl;
		cout << "Note! that your measured borders sizes in the config wont be equal to what you measured. we add the border size of the barcode to take into account the middle of the barcode" << endl;
	}
}


int main(int argc, char** argv)
{
	CmdLineParser cml(argc, argv);
	string calibCamera;
	string configFile;
	string arenaConfigFile;
	string cameraJsonFile;

	if (argc < 2 || cml["-h"])
	{
		cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml] [-arenaPath arenaConfigFile.yml] [-cameraJson cameraJsonFile.json]" << std::endl;
		return 0;
	}
	if (cml["-c"])
		calibCamera = cml("-c");
	if (cml["-config"])
		configFile = cml("-config");
	if (!cml["-arenaPath"])
		 throw std::runtime_error("Didn't set -arenaPath");
	arena_Config_File = cml("-arenaPath");
		if (arena_Config_File.empty())
			std::runtime_error("arena Path is null");
	if (cml["-cameraJson"])
		cameraJsonFile = cml("-cameraJson");
	VideoSource* vs;

	try {
		int camIndex = std::stoi(argv[1]);
		vs = new VideoFile(camIndex, calibCamera, cameraJsonFile);
	}
	catch (const cv::Exception e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
	}
	catch (const std::exception& e) {
		vs = new VideoFile(argv[1], calibCamera, cameraJsonFile);
	}
	
	if (!vs->isOpened()) throw std::runtime_error("Could not open video");
	cout<< "Make sure that when you save the arena configuration file,everything is in GREEN. Explained in README."<<endl;
	cout << "Save the arena config by clicking the " << SAVE_CONFIG_KEY << " key whenever you like"  << endl;
	Detector dd(configFile, vs, OnFrame);
	dd.Start();
	return 0;
}

