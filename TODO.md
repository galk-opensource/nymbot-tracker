
# Immediate Tasks


# Things to fix and do

- [ ] The tracker should really be independent of nymbot-transmitter.  Right now there are several dependencies on the transmitter, for software that should be moved to nymbot-workspace.
  - [X] As first step, the two detectors should not include <nymbot-transmitter.h>, and do not depend on the nymbot-infrastructure lib
  - [ ] As second step, compilation should be conditional, so that if no nymbot-infrastructure lib exists, applications that depend on it do not compile (issue warnings), but the rest do.
- [ ] Both detectors are skipping frames. Lots of frames. the "all_robots" movie has 182 frames. Both detectors see 47.
- [ ] README.md should be broken down into smaller pieces, which are in the doc.  It is too long.
  - [ ] Camera installation and calibration should be done in a separate file
  - [ ] Aruco installation and calibration should done in a separate file
  - [ ] Improve instructions: default_aruco.yml is now in root, and works with movies.  
  - [ ] No need for "-c", as the -config points to it.  But check if "-c" overrides file in -config
- [ ] There is a potential bug, which does not seem to break out yet.  In thirdParty/aruco3.1.12/src/dcf/dcfmarkertracker.cpp, the original code has been commented out (Line 80, marked "TODO: BUG")
  Eyal B. says the code works, but I am worried about this.
- [ ] thirdParty/aruco3.1.12/src/cameraparameters.cpp line 231 is setting the FPS from the config file, but the config file that we use don't have it.
  Eyal B. wrote:  "The only place it begin used anyway is in cout in Detector.cpp but its commented out.  There is also another Fps vairable in aruco with the same namespace(aruco)."
  This is also a potential bug. Is it possible that we are reducing the FPS somehow by not setting it?
- [ ] thirdParty/aruco3.1.12/src/dcf/trackerimpl.cpp Line 320: why was the check changed from "<" to "<="





 

