# **nymbot-tracker README**

_The README in nymbot-workspace is the starting point for the nymbot project._

It is recommended to start reading from there, working top-to-bottom starting at the nymbot-workspace.

## Overview

nymbot-tracker is a C++ library and software for tracking and reporting moving objects(robots, locusts, etc.) that move in a designated arena and tracks them using the Aruco marker attached to their backs. The library has been completely implemented in C++11 with the cmake project manager.

### **Preliminaries**

The top-level directory layout

```shell
    ├── src                             # Core library source files
    ├── app                             # Applications files
    ├── config
    │   ├── arenaConfigs                # arena Configuration files
    │   ├── calibration                 # Camera calibration files
    │   ├── dictionaries                # markers dictionaries+barcodes
    │   └── arucoConfigs                # Aruco library configuration
    ├── thirdParty                      # Third Party
    └── README.md
```

## Documentation

The documentation is maintained in the [nymbot-tracker wiki](https://bitbucket.org/galk-opensource/nymbot-tracker/wiki/Home)

## Installation

_nymbot-tracker_ has several dependencies (opencv, g++, cmake, ... ). Full installation instructions for these are in
[doc/INSTALL.md](doc/INSTALL.md). For most users, these include simply running:

```shell
sudo apt install -y cmake g++ libopencv-dev python3-opencv libeigen3-dev libegl1-mesa-dev
```

Then, build the project from the project root (nymbot-tracker/):

```shell
cmake .
make -k
```

There may be errors in the compilation, if your computer does not have the nymbot-infrastructure library, which is used to control
the nymbot robots. This is fine for an initial installation, or if the tracker is going to be used to track moving objects, without
trying to control any robots.

## Use

Please consult the full [documentation on the wiki](https://bitbucket.org/galk-opensource/nymbot-tracker/wiki/Home)

## **License**

### **Third-Party Software**

This project uses the following third-party software, which is subject to their own licenses:

- [OpenCV](https://opencv.org/) camera calibration library (Apache License, Version 2.0)
- [Aruco](https://sourceforge.net/projects/aruco/) fiducial markers recognition, v3.1.12

### **Attribution**

The camera_calibration.cpp code in this project is adapted from the OpenCV library, developed by [OpenCV team](https://opencv.org/team/), licensed under the Apache License, Version 2.0. See [camera_calibration.cpp](app/camera_calibration.cpp) for the original source code
