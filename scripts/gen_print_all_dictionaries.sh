#!/bin/bash

# Variables for dictionary generation
ARENA_NUMBER_OF_MARKERS=4
ARENA_MARKER_PIXELS=4
ROBOT_NUMBER_OF_MARKERS=100
ROBOT_MARKER_PIXELS=4

# Variables for dictionary printing
ARENA_BARCODE_SIZE_CM=3
ROBOT_BARCODE_SIZE_CM=0.7


# Path variables for dictionares and printables
ROBOT_DICTIONRY_PATH="../config/dictionaries/default_robots_dict_5X5_100barcodes_generated_07"
ROBOT_PRINTABLE_PATH="../config/dictionaries/default_robots_printable_5X5_100barcodes_generated_07.png"
ARENA_DICTIONRY_PATH="../config/dictionaries/default_arena_dict_4X4_4barcodes_generated_07"
ARENA_PRINTABLE_PATH="../config/dictionaries/default_arena_printable_4X4_4barcodes_generated_07.png"
cd ../bin/ || exit
# Usage: dictionary_size  ,marker_size ,output_dictionary_file_path_


# Generate arena dictionary
if ./generate_dictionary ${ARENA_NUMBER_OF_MARKERS} ${ARENA_MARKER_PIXELS} ${ARENA_DICTIONRY_PATH}; then
    echo "Arena dictionary generating SUCCESSED"
else
    echo "Arena dictionary generating FAILED"
fi
# Generate robot dictionary
if ./generate_dictionary ${ROBOT_NUMBER_OF_MARKERS} ${ROBOT_MARKER_PIXELS} ${ROBOT_DICTIONRY_PATH}; then
    echo "Robot dictionary generating SUCCESSED"
else
    echo "Robot dictionary generating FAILED"
fi


# Usage: dictionary_file_path output_image_path  marker_size(cm) 
# Generate arena printable
if ./print_dictionary ${ARENA_DICTIONRY_PATH} ${ARENA_PRINTABLE_PATH} ${ARENA_BARCODE_SIZE_CM}; then
    echo "Robot printable generating SUCCESSED"
else
    echo "Robot printable generating FAILED"
fi
# Generate robot printable
if ./print_dictionary ${ROBOT_DICTIONRY_PATH} ${ROBOT_PRINTABLE_PATH} ${ROBOT_BARCODE_SIZE_CM}; then
    echo "Robot printable generating SUCCESSED"
else
    echo "Robot printable generating FAILED"
fi
