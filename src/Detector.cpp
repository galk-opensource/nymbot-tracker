#include "Detector.h"
#include "ArenaCalib.h"

#define DETECTOR_TIMER
#define TRACKER

//#define SKIP_DETECT

Detector::Detector(const string& configFile, VideoSource* video, function<void(SceneData)> OnDetectCallback)
	:m_config(configFile), m_video(video), m_OnDetectCallback(OnDetectCallback)
{
	if(!m_config.isOpened())
		throw std::runtime_error("config file not initialize.");
	TheTracker.loadParamsFromFile(m_config.configPath);
	m_sd = SceneData(TheTracker.getDictionaries()[0].size(), video, m_config);
	TheTracker.setParams(video->GetCameraParameters(), m_config.markerSizeMeter);
	keepProcessing = true;

}

Detector::~Detector() {

}

void Detector::Start() {

	
	cv::Mat image;
	while (m_video->GetNextFrame(image) && keepProcessing == true)
	{
		#ifdef DETECTOR_TIMER
			timer.start();
		#endif 
		int framdId = m_video->GetCurrentFrameId();

		m_sd.FrameNo = framdId;

	#ifndef SKIP_DETECT
		#ifdef TRACKER
			auto markers = TheTracker.track(image,m_config.arucoTresholdTrust);
		#else
			auto markers = TheTracker.getDetector().detect(image);
		#endif
		
		for (const auto& t : markers) {

			#ifdef TRACKER

			int markerID = t.second->getMarker().id;
			m_sd.Locusts[markerID].m_Marker = t.second->getMarker();

			#else
			
			int markerID = t.id;
			m_sd.Locusts[markerID].m_Marker = t;
			
			#endif		

			m_sd.Locusts[markerID].m_LastDetectedFrame = framdId;
		}
		

		m_sd.MarkerDetected = markers.size();
	#else
		m_sd.MarkerDetected = 0;
	#endif

		m_sd.LastFrame = image;


		#ifdef DETECTOR_TIMER
			timer.stop();
			std::cout << "|@ Frame:" << framdId << ", fps:" << 1. / timer.getAvrg() << std::endl;
		#endif

		m_OnDetectCallback(m_sd);
	}
}

void Detector::Stop() {
	keepProcessing = false;
}