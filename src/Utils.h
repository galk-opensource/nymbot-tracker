#pragma once
#include "opencv2/opencv.hpp"
using namespace cv;
class Utils
{
public:
    // Checks if a matrix is a valid rotation matrix.
    static bool IsRotationMatrix(Mat& R);

    // Calculates rotation matrix to euler angles
    // The result is the same as MATLAB except the order
    // of the euler angles ( x and z are swapped ).
    static Point3d RotationMatrixToEulerAngles(Mat& R);
};
