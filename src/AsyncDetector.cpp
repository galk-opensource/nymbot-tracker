#include "AsyncDetector.h"




AsyncDetector::AsyncDetector(const string& configFile, VideoSource* video)
	:Detector(configFile, video, [this](SceneData sd) {OnFrame(sd); }), m_ThreadRun(false)
{}


AsyncDetector::~AsyncDetector() {
	if (m_thread.joinable())
		m_thread.join();
}

void AsyncDetector::Start() {
	m_ThreadRun = true;
	m_thread = std::thread([this]() {
		Detector::Start();
		m_ThreadRun = false;
	});
}

threadsafe_queue<SceneData>& AsyncDetector::GetProcessedFrames() {
	return ProcessedFrames;
}
bool AsyncDetector::IsProcessing() {
	return m_ThreadRun;
}

void AsyncDetector::OnFrame(SceneData sd) {
	ProcessedFrames.push(sd);
}