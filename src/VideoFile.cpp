using namespace std;

#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>
// #include <stdio.h>
#include <linux/videodev2.h>
#include "json.hpp"
#include "VideoFile.h"
#include <fstream>
#include <string>
#include <regex>
using json = nlohmann::json;


/*
	different backends for video capture class.
	The backend is set by commenting out the corresponding line.
	Default is GStreamer,as it is the most stable for now.(18 fps,full picture)
*/
// #define V4L
// #define ARAVIS
#define GSTREAMER


const int FRAME_HEIGHT = 3648;
const int FRAME_WIDTH = 5472;
const int FPS = 100;


void VideoFile::Close()
{
	m_VideoCapture.release();
	m_Playing =m_isOpen = false;
}

bool VideoFile::GetNextFrame(cv::Mat& frame)
{
	m_CurrentFrame.release();
	if (!m_isOpen)
		return false;

	m_Playing = m_VideoCapture.grab();
	while (m_Playing) {
		m_FrameId++;
		/*	if (!mFirstGrab)
				mFrameId++;*/
				//mFirstGrab = false;
		if (m_FrameId % m_VideoInterval == 0) {
			// May try values different than the default(0) for multi-channel cameras
			m_VideoCapture.retrieve(m_CurrentFrame);
			break;
		}
		m_Playing = m_VideoCapture.grab();
	}

	if(!m_CurrentFrame.empty()) {
		#ifdef DEBUG
			cout << "num of channels: " << m_CurrentFrame.channels() << " Type of Mat: "<< m_CurrentFrame.type() << " Depth: " << m_CurrentFrame.depth()  <<endl;
		#endif 

		/*
		----------------------------------------------------------------------------------------------------------------
		We had the problem of getting a tripled image from the camera when using v4l2 as backend.
		The following comments are for reference of what we tried to solve the problem. None of them worked.
		----------------------------------------------------------------------------------------------------------------
		*/

		/* crop image 
		   ---------  */
		/*   
			// cv::Rect rect(0, 0, m_CurrentFrame.cols, m_CurrentFrame.rows);
    		// cv::Mat true_img = m_CurrentFrame(rect).clone();
    		// frame = true_img;
		*/

		/* split channels 
		   --------------  */
		/*
			cv::Mat bgrChannels[3];
			// Bayer algorithms assume single channel grayscale image. So we split the image into channels.
			cv::split(m_CurrentFrame, bgrChannels);
			cv::Mat grayBayerImage = bgrChannels[1]; // Assuming green channel is the Bayer pattern.
			// cout << "num of channels: " << grayBayerImage.channels() << " Type of Mat: "<< grayBayerImage.type() << " Depth: " << grayBayerImage.depth()  <<endl;
			cv::Mat colorImage;
			// frame = colorImage;
			frame = m_CurrentFrame;
		*/

		/* convert to color/gray image 
		   ----------------------------  */
		/*
			cv::Mat grayBayerImage;

			// options: COLOR_BayerRGGB2GRAY, COLOR_BayerBG2GRAY, COLOR_BayerRG2GRAY, COLOR_BayerRGGB2RGB
			cv::cvtColor(grayBayerImage, colorImage, cv::COLOR_BGR2YUV_YV12); // convert to color/gray image.
		*/

		/* decode 
			------ */
		// cv::imdecode(m_CurrentFrame, cv::IMREAD_GRAYSCALE );

		/* default */				
		frame = m_CurrentFrame;
	}
	else{
	 	frame = m_CurrentFrame;
	}
	return !m_CurrentFrame.empty();
}

cv::Mat VideoFile::GetCurrentFrame()
{
	return m_CurrentFrame;
}

bool VideoFile::IsPlaying() const
{
	return m_Playing;
}

unsigned VideoFile::GetCurrentFrameId() const
{
	return m_FrameId;
}

unsigned VideoFile::GetVideoInterval() const
{
	return m_VideoInterval;
}

void VideoFile::SetVideoInterval(unsigned interval)
{
}


bool VideoFile::SetProps(int prop,double value)
{
	return m_VideoCapture.set(prop,value);
}
//Ctor

VideoFile::VideoFile(const string& videoPath, const string calibFile, const string cameraJson)
	:m_VideoPath(videoPath),VideoSource(calibFile), m_cameraJson(cameraJson)
{
	Init();
}



VideoFile::VideoFile(const int portID, const string calibFile, const string cameraJson):m_PortID(portID), VideoSource(calibFile), m_cameraJson(cameraJson)
{
	Init();
}


#ifdef DEBUG 
void VideoFile::printCameraParams(const std::string label, VideoFile& vf)
{
	std::cerr << label << ":"
		<< " #frames: " <<  vf.m_VideoCapture.get(cv::CAP_PROP_FRAME_COUNT)
		<< " fps: " << vf.m_VideoCapture.get(cv::CAP_PROP_FPS) 
		<< " h,w: " << vf.m_VideoCapture.get(cv::CAP_PROP_FRAME_HEIGHT) << ", " <<   vf.m_VideoCapture.get(cv::CAP_PROP_FRAME_WIDTH)

		// https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
		// Format code meaning.
		#ifndef GSTREAMER
			<< " format: " << vf.m_VideoCapture.get(cv::CAP_PROP_FORMAT)
			<< " pxfmt: " << vf.m_VideoCapture.get(cv::CAP_PROP_CODEC_PIXEL_FORMAT)
			<< " prop: " << vf.m_VideoCapture.get(cv::CAP_PROP_MODE)
			<< " 4cc: " << vf.m_VideoCapture.get(cv::CAP_PROP_FOURCC) 
		#endif  

		<< std::endl;

	return ;
}
#else
inline void VideoFile::printCameraParams(const std::string label, VideoFile& vf)
{ return ; }
#endif


void VideoFile::Open()
{
	#ifdef V4L
		#define BACKEND cv::CAP_V4L2
	#elif defined(ARAVIS)
		#define BACKEND cv::CAP_ARAVIS
	#elif defined(GSTREAMER)
		#define BACKEND cv::CAP_GSTREAMER
		 // pipeline: 
		std::ifstream f(m_cameraJson);
		
		json data = json::parse(f);
		string data_str = data.dump();
		data_str = std::regex_replace(data_str, std::regex(":"), "="); 
		data_str = std::regex_replace(data_str, std::regex("\""), ""); 
		data_str = data_str.substr (1, data_str.size()-2);
		
		// const char *gstpipeline = 
		string gstpipeline = 
		// 18FPS
			// "tcambin ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! videoconvert ! appsink";	 // works, but 4sec laetency
			//"tcambin ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! videoconvert n-threads=4  ! appsink";	// works a bit better than above
			// "tcambin ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! queue leaky=2 max-size-time=0 max-size-bytes=0 max-size-buffers=1 ! videoconvert n-threads=4  ! appsink"; // works about 2sec delay
			//  "tcambin tcam-properties=tcam,ExposureAuto=Off,ExposureTime=4000,GainAuto=Off,Gain=27.0 serial=41020357 ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! queue leaky=2 max-size-time=0 max-size-bytes=0 max-size-buffers=1 ! videoconvert n-threads=4  ! appsink"; // Better smoothiness the lower exposure time is. delay reduced by a little.
			// "tcambin tcam-properties=tcam,ExposureAuto=Off,ExposureTime=170000,GainAuto=Off,Gain=0 serial=41020357 ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! videoconvert n-threads=4  ! appsink"; // tis pipeline
			// "tcambin ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! nvvidconv ! appsink"; // does not work, 1 sec delay  https://forums.developer.nvidia.com/t/the-delay-of-gstreamer-pipeline-in-opencv-videocapture/59713
			std::string(cv::format("tcambin tcam-properties=tcam,%s serial=41020357 ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! queue leaky=2 max-size-time=0 max-size-bytes=0 max-size-buffers=1 ! videoconvert n-threads=4  ! appsink", data_str.c_str()));
			// "tcambin tcam-properties=tcam,{%s} serial=41020357 ! video/x-raw,format=BGRx,width=5472,height=3648,framerate=18/1 ! queue leaky=2 max-size-time=0 max-size-bytes=0 max-size-buffers=1 ! videoconvert n-threads=4  ! appsink", data_str;
		// cout << gstpipeline << std::endl;
	#elif defined(FFMPEG)
		#define BACKEND cv::CAP_FFMPEG
	#endif 


	if(m_VideoPath.empty()) {
			#ifdef GSTREAMER
				if (!m_VideoCapture.open(gstpipeline, BACKEND)) {
					std::cerr << "Failed to open camera." << std::endl;
					exit(-1);
				}
			#else
				if (!m_VideoCapture.open(m_PortID, BACKEND)) {
					std::cerr << "Failed to open camera." << std::endl;
					exit(-1);
				}
			#endif
		#ifdef DEBUG
			std::cerr << "openning port " << m_PortID << m_VideoCapture.getBackendName() << endl;
		#endif

	}
	else {
		m_VideoCapture.open(m_VideoPath);
		m_PortID = -1;
	}

	m_Playing =m_isOpen = m_VideoCapture.isOpened();
}


void VideoFile::Init()
{
	Open();

	#ifdef DEBUG
//	std::cerr << cv::getBuildInformation() << std::endl;
	#endif	

	printCameraParams("default", *this);

 	//m_VideoCapture.set(cv::CAP_PROP_MODE,cv::CAP_MODE_RGB);  // We don't know what this does.

	#ifdef V4L
		/* 8 buit formats: accordin // gives 6 fps, defaultg to doc:   GRBG, RGGB, GBRG or BA81.  */
		 cout << "Setting PROP_FOURCC: " << m_VideoCapture.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('G','R','B','G')) << endl; // gives 6 fps, default
		 //cout << "Setting PROP_FOURCC: " << m_VideoCapture.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('G','B','R','G')) << endl;  // gives 6 fps, default
		 // cout << "Setting PROP_FOURCC: " << m_VideoCapture.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('B','A','8','1')) << endl; // gives 6 fps, default

 		// cout << "Setting PROP_FOURCC: " << m_VideoCapture.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('R','G','G','B')) << endl; // gives 18 fps but triplicated image


		/* 12 bit formats*/

		/* 16 but formats */
		// cout << "Setting PROP_FOURCC: " << m_VideoCapture.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('R','G','1','6')) << endl;
	#elif defined(ARAVIS)
		// 8 bit
		cout << "Setting PROP_FORMAT: " << m_VideoCapture.set(cv::CAP_PROP_FORMAT,cv::CAP_PVAPI_PIXELFORMAT_BAYER8) << endl; // Bayer8
		
		cout << "Setting PROP_CODEC_PIXEL_FORMAT: " << m_VideoCapture.set(cv::CAP_PROP_CODEC_PIXEL_FORMAT,cv::CAP_PVAPI_PIXELFORMAT_BAYER8) << endl; 
		
	#endif

	#ifndef GSTREAMER
		cout << "Setting PROP_CHANNEL: " << m_VideoCapture.set(cv::CAP_PROP_CHANNEL,0) << endl;
		//m_VideoCapture.set(cv::CAP_PROP_CONVERT_RGB, true);

		cout << "Setting FRAME_HEIGHT: " <<	m_VideoCapture.set(cv::CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT) << endl ; 
		cout << "Setting FRAME_WIDTH: " << m_VideoCapture.set(cv::CAP_PROP_FRAME_WIDTH,FRAME_WIDTH) << endl;
		cout << "Setting FPS: " <<	m_VideoCapture.set(cv::CAP_PROP_FPS,FPS) << endl;
	#endif

	printCameraParams("after setting", *this);


	m_Height = m_VideoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);
	m_Width = m_VideoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
	
	m_NumFrames = m_VideoCapture.get(cv::CAP_PROP_FRAME_COUNT);
	m_Fps = m_VideoCapture.get(cv::CAP_PROP_FPS);

	/*
		We had problem of getting wrong resolution from the camera.
		Now it is solved by using a specific backend and setting the pipeline/props manually.
		Saved for future reference of what not using backend demands.
	*/
	// For unknown reasons, setting the resolution above does not hold inside opencv when calling get as below. but for some reason it still must be done to make changes.
	// The default video resolution is in wrong dimensions. We don't know why exactly.
	// For now the fix is to hard code for the program the right dimensions as getting them with get() doesn't return the right value EVEN SO that set() is a must and does effect opencv resolution. 
		// m_Height = FRAME_HEIGHT;
  		// m_Width = FRAME_WIDTH;

}
void VideoFile::SkipFrame()
{
	if (!m_Playing)
		return;

	m_Playing = m_VideoCapture.grab();
	m_FrameId++;
}

void VideoFile::Rewind()
{
	m_VideoCapture.set(cv::CAP_PROP_POS_FRAMES, 0);
}


std::string VideoFile::GetVideoPath() const
{
	return m_VideoPath;
}

unsigned VideoFile::GetNumFrames() const
{
	return m_NumFrames;
}

// Frames will be saved as frameX.jpg where X is number of times function was called.
// By default,path is "../"
void VideoFile::saveFrameToImage(cv::Mat frame,const string path){
	static int frameId=0;
	// Save the frame to a file
	std::string filename = "frame" + std::to_string(frameId) + ".png";
	cv::imwrite(path+filename, frame);
	frameId++;
}

//void VideoFile::Rewind()
//{
//	m_VideoCapture.open(mPath);
//	if (mVideoCapture.open(mPath))
//	{
//		std::cout << "Video file open " << std::endl;
//	}
//	else
//	{
//		std::cout << "Not able to Video file open " << std::endl;
//
//	}
//	mFrameId = 0;
//	mFirstGrab = true;
//	mPlaying = true;
//}
