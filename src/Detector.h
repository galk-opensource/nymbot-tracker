﻿#ifndef DEF_DETECTOR
#define DEF_DETECTOR
#include <iostream>
#include <functional>
#include "opencv2/opencv.hpp"
#include "Config.h"
#include "VideoSource.h"
#include "SceneData.h"
#include "Locust.h"
#include "dcf/dcfmarkertracker.h"
#include "threadsafe_queue.h"

using namespace std;
struct   TimerAvrg { std::vector<double> times; size_t curr = 0, n; std::chrono::high_resolution_clock::time_point begin, end;   TimerAvrg(int _n = 30) { n = _n; times.reserve(n); }inline void start() { begin = std::chrono::high_resolution_clock::now(); }inline void stop() { end = std::chrono::high_resolution_clock::now(); double duration = double(std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) * 1e-6; if (times.size() < n) times.push_back(duration); else { times[curr] = duration; curr++; if (curr >= times.size()) curr = 0; } }double getAvrg() { double sum = 0; for (auto t : times) sum += t; return sum / double(times.size()); } };

class Detector
{
public:
	Detector(const string& configFile, VideoSource* video, function<void(SceneData)> OnDetectCallback);
	virtual ~Detector();
	void Start();
	void Stop();
private:

	Config m_config;
	VideoSource* m_video;
	function<void(SceneData)> m_OnDetectCallback;
	aruco::DFCMarkerTracker TheTracker;
	SceneData m_sd;
	bool keepProcessing;
	//debug	
	TimerAvrg timer;
};


#endif
