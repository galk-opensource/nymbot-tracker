#ifndef DEF_SCENEDATA
#define DEF_SCENEDATA
#include "opencv2/opencv.hpp"
#include "Locust.h"
#include <vector>
#include "VideoSource.h"
#include "Config.h"
#include "ArenaCalib.h"

class SceneData
{
	public :
		SceneData();
		SceneData(size_t no_marker,VideoSource* video,Config config);

		cv::Point2d GetPositionXY(Locust l);
		cv::Mat GetMap();


		std::vector<Locust> Locusts;
		unsigned int FrameNo;
		unsigned int MarkerDetected;
		cv::Mat LastFrame;
		VideoSource* video;
		Config config;

private:
	void InitArenaCalibration();
};


#endif
