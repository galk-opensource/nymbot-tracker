#include "SceneData.h"
#include "ArenaCalib.h"

SceneData::SceneData() {}

SceneData::SceneData(size_t no_marker, VideoSource* video, Config config)
	:FrameNo(0), Locusts(no_marker), video(video), MarkerDetected(0), config(config)
{
	
}

cv::Point2d SceneData::GetPositionXY(Locust l)
{

	return l.GetPositionIJ();
}

cv::Mat SceneData::GetMap()
{
	cv::Mat map(cv::Mat::ones(cv::Size(video->m_Width, video->m_Height), CV_32SC1)*-1);
	
	for (const auto& l: Locusts)
	{
		if (!l.m_Marker.isValid() || l.m_LastDetectedFrame != FrameNo)
			continue;
		map.at<int>(l.m_Marker.getCenter()) = l.m_Marker.id; // TODO: what does this do?
	}
	return map;
}

