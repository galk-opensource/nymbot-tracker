#include "ArenaCalib.h"

ArenaCalib::ArenaCalib(const Config& config, aruco::CameraParameters CamParam)
	:cameraParams(CamParam)
{
	configFile = config.configPath;
	m_ArenaWidth_cm = config["arena_width_Meter"] ;
	m_ArenaWidth_cm *= 100.0 ;
	m_ArenaHeight_cm = config["arena_height_Meter"];
	m_ArenaHeight_cm *= 100.0;
	m_SizeMarker_cm = config["arena_marker_size_Meter"];
	m_SizeMarker_cm *= 100.0;
}

bool ArenaCalib::Calib(cv::Mat InImage) {
	cv::FileStorage fs;

	MarkerDetector MDetector;
	MDetector.loadParamsFromFile(configFile);
	double MarkerSize = m_SizeMarker_cm / 100.0; // convert cm to meter
	vector<Marker> currMarkers = MDetector.detect(InImage, cameraParams, MarkerSize);
	cout<< "Detected "<<currMarkers.size()<<" barcodes"<<endl;
	if (currMarkers.size() != 4)
	{
		cerr << "arena calibration failed." << endl;
		return false;
	}
	Markers.resize(4);

	//sort marker
	for (size_t i = 0; i < currMarkers.size(); i++)
		Markers[currMarkers[i].id] = currMarkers[i];
	cv::Point2i p0 = Markers[0].contourPoints[0];
	cv::Point2i p1 = Markers[1].contourPoints[1];
	cv::Point2i p2 = Markers[2].contourPoints[2];
	cv::Point2i p3 = Markers[3].contourPoints[3];
	cv::Point2d widthdiff = p1 - p0;
	cv::Point2d heightdiff = p2 - p1;
	double widthpx = std::norm(std::complex< double >(widthdiff.x, widthdiff.y));
	double heightpx = std::norm(std::complex< double >(heightdiff.x, heightdiff.y));
	double wpx2m = widthpx / m_ArenaWidth_cm;
	double hpx2m = heightpx / m_ArenaHeight_cm;
	m_Px2cm = cv::Point2d(wpx2m, hpx2m);
	m_ArenaContur.push_back(p0);
	m_ArenaContur.push_back(p1);
	m_ArenaContur.push_back(p2);
	m_ArenaContur.push_back(p3);
	return true;
}

bool ArenaCalib::IsCalibrated()
{
	return m_ArenaContur.size()== 4;
}
