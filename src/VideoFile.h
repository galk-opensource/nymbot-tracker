#ifndef DEF_VIDEO_FILE
#define DEF_VIDEO_FILE

using namespace std;

#include "VideoSource.h"

class VideoFile : public VideoSource
{
public:
	// Inherited via VideoSource
	virtual void Open() override;
	virtual void Close() override;
	bool GetNextFrame(cv::Mat& frame) override;
	virtual cv::Mat GetCurrentFrame() override;
	virtual bool IsPlaying() const override;
	virtual unsigned GetCurrentFrameId() const override;
	virtual unsigned GetVideoInterval() const override;
	virtual void SetVideoInterval(unsigned interval) override;
	bool SetProps(int prop,double value);

	virtual void SkipFrame() override;
	virtual void Rewind() override;

	void saveFrameToImage(cv::Mat frame,const string path = "../");

	//Ctor
	VideoFile(const string& videoPath, const string calibFile = "", const string cameraJson = "");
	void Init();
	VideoFile(const int portID, const string calibFile = "", const string cameraJson = "");


	std::string GetVideoPath() const;
	unsigned GetNumFrames() const;

private:
	string m_VideoPath;
	string m_cameraJson;
	int m_PortID = -1;
	unsigned m_NumFrames;
	cv::VideoCapture m_VideoCapture;
	void printCameraParams(const std::string label,VideoFile &vf);


};


#endif
