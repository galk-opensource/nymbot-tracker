#pragma once

#include <iostream>
#include <functional>
#include "Detector.h"
#include <thread>
using namespace std;
class AsyncDetector : public Detector
{
public:
	AsyncDetector(const string& configFile, VideoSource* video);
	virtual ~AsyncDetector();

	void Start();
	threadsafe_queue<SceneData>& GetProcessedFrames();

	bool IsProcessing();
private:
	void OnFrame(SceneData sd);
	
	
	threadsafe_queue<SceneData> ProcessedFrames;
	std::thread m_thread;
	bool m_ThreadRun;



};
