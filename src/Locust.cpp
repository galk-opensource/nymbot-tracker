#include "Locust.h"
#include "Utils.h"

Locust::Locust()
{

}

Locust::Locust(aruco::Marker marker) :m_Marker(marker), m_LastDetectedFrame(-1)
{
}

cv::Point Locust::GetPositionIJ()
{
	return m_Marker.getCenter();
}


int Locust::GetOrientation()
{
    if (!m_Marker.isValid())
        return -999;
    Point p1 = m_Marker[1],
        p2 = m_Marker[0];
    float angle = atan2(p1.y - p2.y, p1.x - p2.x);
    angle *= 180 / CV_PI;
    return ((int)angle+360)%360;
	
    //return Utils::RotationMatrixToEulerAngles(m_Marker.Rvec);
}
