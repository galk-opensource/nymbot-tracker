#pragma once

#include <opencv2/core/core.hpp>
#include "aruco.h"

struct Config
{
public:
	Config() {};
	Config(const std::string configFile);
	bool isOpened();

	cv::FileNode operator[](const char* nodename) const;
	
	std::string configPath;
	float arucoTresholdTrust;
	float markerSizeMeter;
private:
	void Read();

	template<typename Type>
	bool attemtpRead(const std::string& name, Type& var, cv::FileStorage& fs, Type defaultType);

	cv::FileStorage fs;
};
