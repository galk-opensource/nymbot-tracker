#ifndef DEF_LOCUST
#define DEF_LOCUST

#include "aruco.h"


class Locust
{
	public :
		Locust();
		Locust(aruco::Marker marker);
		cv::Point GetPositionIJ();
		int GetOrientation();
		aruco::Marker m_Marker;
		int m_LastDetectedFrame;
};


#endif
