#pragma once
#include "Config.h"
#include "markerdetector.h"
#include <iostream>
#include <vector>
using namespace std;
using namespace aruco;
class ArenaCalib
{
public:
	ArenaCalib():m_ArenaHeight_cm(-1.), m_ArenaWidth_cm(-1.){};
	ArenaCalib(const Config& config, aruco::CameraParameters CamParam);

	/*
	*	(tl)	(tr)
	*	 0		1
	*	---------
	*	|		|
	*	|		|
	*	---------
	*	2		3
	*	(bl)	(br)
	*/
	bool Calib(cv::Mat InImage);
	bool IsCalibrated();
	cv::Point2d m_Px2cm;
	vector<cv::Point2i> m_ArenaContur;
	float m_ArenaWidth_cm;
	float m_ArenaHeight_cm;
	float m_SizeMarker_cm;
	string m_dict_path;
private:
	string configFile;
	vector<Marker> Markers;
	aruco::CameraParameters cameraParams;
};
