#include "VideoSource.h"

void VideoSource::SetVideoInterval(unsigned interval)
{
	m_VideoInterval = interval;
}

VideoSource::VideoSource(const std::string calibFile) :m_Playing(false), m_CalibFile(calibFile), m_isOpen(false)
{
    m_VideoInterval = 1;
    m_FrameId = 0;
    if (!m_CalibFile.empty())
        m_CameraParameter.readFromXMLFile(m_CalibFile);
}

aruco::CameraParameters VideoSource::GetCameraParameters()
{
    return m_CameraParameter;
}

bool VideoSource::isOpened() {
    return m_isOpen;
}
