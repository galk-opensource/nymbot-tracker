#include "Config.h"

Config::Config(const std::string configFile):configPath(configFile)
{
	if(configFile.empty())
		return;

	fs.open(configFile, cv::FileStorage::READ);
	if (!fs.isOpened())
		return;
	Read();
}

bool Config::isOpened() {
	return fs.isOpened();
}

void Config::Read() {
	attemtpRead("aruco-trackingTresholdTrust", arucoTresholdTrust, fs, 0.3f);
	attemtpRead("aruco-markerSizeMeter", markerSizeMeter, fs, 0.08f);
}


template<typename Type>
bool Config::attemtpRead(const std::string& name, Type& var, cv::FileStorage& fs, Type defaultType) {
	if (fs[name].type() != cv::FileNode::NONE) {
		fs[name] >> var;
		return true;
	}

	var = defaultType;
	return false;
}

cv::FileNode Config::operator[](const char* nodename) const {
	return fs[nodename];
}
