#pragma once
#include <iostream>

using namespace std;

class CmdLineParser {
	int argc;
	char** argv;

public:
	CmdLineParser(int _argc, char** _argv);
	bool operator[](string param);
	string operator()(string param, string defvalue = "-1");
};

