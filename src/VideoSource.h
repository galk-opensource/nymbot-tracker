#pragma once

using namespace std;

#include <opencv2/opencv.hpp>
#include "cameraparameters.h"


class VideoSource
{
public:
    virtual void Open() =0;
    virtual void Close() =0;
    virtual bool GetNextFrame(cv::Mat& frame) = 0;
    virtual cv::Mat GetCurrentFrame() = 0;
    virtual bool IsPlaying() const = 0;
    virtual unsigned GetCurrentFrameId() const = 0;
    virtual unsigned GetVideoInterval() const = 0;
    virtual void SetVideoInterval(unsigned interval) = 0;
    virtual void SkipFrame() = 0;
    virtual void Rewind() = 0;

    VideoSource(const std::string calibFile = "");
    aruco::CameraParameters GetCameraParameters();
    bool isOpened();
    //void InitMetadata();

    //VIDEO METADATA
    int m_Fps;
    double m_Height;
    double m_Width;


protected:
    unsigned m_VideoInterval;
    bool m_Playing;    
    bool m_isOpen;
    unsigned m_FrameId;
    cv::Mat m_CurrentFrame;
    std::string m_CalibFile;
    aruco::CameraParameters m_CameraParameter;

};
